﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Data.SqlClient;
using System.Diagnostics;

namespace BookStore
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent(); 
            
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            progressBar1.Visible = false;
            ConnectionDB.connectsql();
            this.Cursor = Cursors.Default;
        }

        private  DataTable data = null;

        private bool blockSQLInjection()
        {
            //block Sql Injection
            string x;
            for (int i = 0; i < tb_username.Text.Length; i++)
            {
                x = tb_username.Text[i].ToString();
                if (x.Equals("\"") || x.Equals("\'"))
                {
                    return true;
                }
            }
            for (int i = 0; i < tb_password.Text.Length; i++)
            {
                x = tb_password.Text[i].ToString();
                if (x.Equals("\"") || x.Equals("\'"))
                {
                    return true;
                }
            }
            return false;
        }


        private async void checkLogin()
        {
            //block Sql Injection
            if (blockSQLInjection())
            {
                MessageBox.Show("ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง!!", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                clear();
                return;
            }


            // loading...
            progressBar1.Visible = true;

            this.Cursor = Cursors.WaitCursor;
            tb_username.Enabled = false;
            tb_password.Enabled = false;
            textUsername.Enabled = false;
            textPassword.Enabled = false;
            btnLogin.Enabled = false;
            textLogin.Enabled = false;

            await Task.Run(() => connect());

            tb_username.Enabled = true;
            tb_password.Enabled = true;
            textUsername.Enabled = true;
            textPassword.Enabled = true;
            btnLogin.Enabled = true;
            textLogin.Enabled = true;
            this.Cursor = Cursors.Default;
            
            // error cannot connection database
            if(data == null)
            {
                progressBar1.Visible = false;
                MessageBox.Show("ไม่สามารถเชื่อมต่อฐานข้อมูลได้ กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ต", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //finish load
            progressBar1.Visible = false;


            if (data.Rows.Count > 0)
            {
                MessageBox.Show("ยินดีต้อนรับคุณ " + data.Rows[0][4] + "  " + data.Rows[0][5] + " เข้าสู่ระบบ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //check Admin
                if (data.Rows[0][0].ToString()=="0000000000000")
                {
                    // Menu1 Admin
                    /* exit with me
                    this.Hide();
                    var menu = new MenuAdmin();
                    menu.Closed += (s, args) => this.Close();
                    menu.Show();

                    //show username in menu
                    menu.textUserName.Text = tb_username.Text;
                    */

                    this.Hide();
                    MenuAdmin mnAdmin = new MenuAdmin();


                    //show username in menu
                    mnAdmin.textUserName.Text = tb_username.Text;
                    mnAdmin.ShowDialog();
                    clear();
                    CenterToScreen();
                    this.Show();
                }
                else
                {
                    // Menu2 User
                    this.Hide();
                    MenuShop mn = new MenuShop();

                    //show username in menu
                    mn.textUserName.Text = tb_username.Text;
                    mn.ShowDialog();
                    clear();
                    CenterToScreen();
                    this.Show();
                }
            }
            else
            {
                MessageBox.Show("ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง!!", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                clear();
            }
            

        }

        private void clear()
        {
            tb_password.Clear();
            tb_username.Clear();
            textPassword.Show();     //bug password
            this.ActiveControl = tb_username;

            // bug color button
            btnLogin.BackColor = mpl;
            btnLogin.BorderColor = Color.Black;
            textLogin.BackColor = mpl;
        }

        private void connect()
        {
            data = ConnectionDB.executeSQL("SELECT * FROM UserShop WHERE ID = '" + tb_username.Text + "' AND Password = '" + tb_password.Text + "'");
        }


        private void enter_login(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                checkLogin();
            }
        }

        private void Username_Enter(object sender, EventArgs e)
        {
            textUsername.Hide();
        }

        private void Username_Leave(object sender, EventArgs e)
        {
            if(tb_username.Text == "")
            {
                textUsername.Show();
            }
            
        }

        private void Password_Enter(object sender, EventArgs e)
        {
            textPassword.Hide();
        }

        private void Password_Leave(object sender, EventArgs e)
        {
            if(tb_password.Text == "")
            {
                textPassword.Show();
            }
            
        }

        private void Click_Username(object sender, EventArgs e)
        {
            textUsername.Hide();
            tb_username.Focus();
        }

        private void Click_Pawword(object sender, EventArgs e)
        {
            textPassword.Hide();
            tb_password.Focus();
        }

        private void textLogin_Click(object sender, EventArgs e)
        {
            checkLogin();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            checkLogin();
        }

        private void textExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // button color change
        private static Color mp = Color.LimeGreen;
        private static Color mpl = Color.LightSlateGray;
        private void mpLogin(object sender, EventArgs e)
        {
            mp = Color.LimeGreen;
            btnLogin.BackColor = mp;
            btnLogin.BorderColor = mp;
            textLogin.BackColor = mp;
        }

        private void mplLogin(object sender, EventArgs e)
        {
            btnLogin.BackColor = mpl;
            btnLogin.BorderColor = Color.Black;
            textLogin.BackColor = mpl ;
        }

        private void mpExit(object sender, EventArgs e)
        {
            mp = Color.Red;
            btnExit.BackColor = mp;
            btnExit.BorderColor = mp;
            textExit.BackColor = mp;
        }

        private void mplExit(object sender, EventArgs e)
        {
            btnExit.BackColor = mpl;
            btnExit.BorderColor = Color.Black;
            textExit.BackColor = mpl;
        }

        // to move panel when click panel
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void MoveForm(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

    }
}
