﻿namespace BookStore
{
    partial class Sales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sales));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpHeader = new System.Windows.Forms.TableLayoutPanel();
            this.pictLogo = new System.Windows.Forms.PictureBox();
            this.pictNameShop = new System.Windows.Forms.PictureBox();
            this.tblDocHeader = new System.Windows.Forms.TableLayoutPanel();
            this.tblDoc = new System.Windows.Forms.TableLayoutPanel();
            this.tbDocID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tblUserNameHeader = new System.Windows.Forms.TableLayoutPanel();
            this.textUserName = new System.Windows.Forms.Label();
            this.pictFormName = new System.Windows.Forms.PictureBox();
            this.tblBody = new System.Windows.Forms.TableLayoutPanel();
            this.dgvSales = new System.Windows.Forms.DataGridView();
            this.ISBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblInputMain = new System.Windows.Forms.TableLayoutPanel();
            this.tblInput = new System.Windows.Forms.TableLayoutPanel();
            this.textGetM = new System.Windows.Forms.Label();
            this.textQuantity = new System.Windows.Forms.Label();
            this.textProductID = new System.Windows.Forms.Label();
            this.tbCusID = new System.Windows.Forms.TextBox();
            this.tbProductID = new System.Windows.Forms.TextBox();
            this.tbGetMoney = new System.Windows.Forms.TextBox();
            this.textCusID = new System.Windows.Forms.Label();
            this.nupQty = new System.Windows.Forms.NumericUpDown();
            this.textDiscount = new System.Windows.Forms.Label();
            this.tblButtonInput = new System.Windows.Forms.TableLayoutPanel();
            this.pictAdd = new System.Windows.Forms.PictureBox();
            this.pictDelete = new System.Windows.Forms.PictureBox();
            this.pictCheckBill = new System.Windows.Forms.PictureBox();
            this.tblFooter = new System.Windows.Forms.TableLayoutPanel();
            this.TextTotal = new System.Windows.Forms.Label();
            this.tbReturnMoney = new System.Windows.Forms.TextBox();
            this.pictReport = new System.Windows.Forms.PictureBox();
            this.pictHome = new System.Windows.Forms.PictureBox();
            this.textReM = new System.Windows.Forms.Label();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tlpMain.SuspendLayout();
            this.tlpHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictNameShop)).BeginInit();
            this.tblDocHeader.SuspendLayout();
            this.tblDoc.SuspendLayout();
            this.tblUserNameHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictFormName)).BeginInit();
            this.tblBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).BeginInit();
            this.tblInputMain.SuspendLayout();
            this.tblInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupQty)).BeginInit();
            this.tblButtonInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictCheckBill)).BeginInit();
            this.tblFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictHome)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.Color.LightSkyBlue;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpHeader, 0, 0);
            this.tlpMain.Controls.Add(this.tblBody, 0, 1);
            this.tlpMain.Controls.Add(this.tblFooter, 0, 2);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpMain.Size = new System.Drawing.Size(944, 501);
            this.tlpMain.TabIndex = 1;
            // 
            // tlpHeader
            // 
            this.tlpHeader.ColumnCount = 3;
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpHeader.Controls.Add(this.pictLogo, 0, 0);
            this.tlpHeader.Controls.Add(this.pictNameShop, 1, 0);
            this.tlpHeader.Controls.Add(this.tblDocHeader, 2, 0);
            this.tlpHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHeader.Location = new System.Drawing.Point(3, 3);
            this.tlpHeader.Name = "tlpHeader";
            this.tlpHeader.RowCount = 1;
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHeader.Size = new System.Drawing.Size(938, 104);
            this.tlpHeader.TabIndex = 0;
            // 
            // pictLogo
            // 
            this.pictLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictLogo.Image = global::BookStore.Properties.Resources.logo_สีขาว_png;
            this.pictLogo.Location = new System.Drawing.Point(3, 3);
            this.pictLogo.Name = "pictLogo";
            this.pictLogo.Size = new System.Drawing.Size(134, 98);
            this.pictLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictLogo.TabIndex = 0;
            this.pictLogo.TabStop = false;
            // 
            // pictNameShop
            // 
            this.pictNameShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictNameShop.Image = global::BookStore.Properties.Resources.bookshop;
            this.pictNameShop.Location = new System.Drawing.Point(143, 3);
            this.pictNameShop.Name = "pictNameShop";
            this.pictNameShop.Size = new System.Drawing.Size(322, 98);
            this.pictNameShop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictNameShop.TabIndex = 1;
            this.pictNameShop.TabStop = false;
            // 
            // tblDocHeader
            // 
            this.tblDocHeader.ColumnCount = 1;
            this.tblDocHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblDocHeader.Controls.Add(this.tblDoc, 0, 1);
            this.tblDocHeader.Controls.Add(this.tblUserNameHeader, 0, 0);
            this.tblDocHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblDocHeader.Location = new System.Drawing.Point(471, 3);
            this.tblDocHeader.Name = "tblDocHeader";
            this.tblDocHeader.RowCount = 2;
            this.tblDocHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62F));
            this.tblDocHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38F));
            this.tblDocHeader.Size = new System.Drawing.Size(464, 98);
            this.tblDocHeader.TabIndex = 2;
            // 
            // tblDoc
            // 
            this.tblDoc.ColumnCount = 2;
            this.tblDoc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblDoc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tblDoc.Controls.Add(this.tbDocID, 1, 0);
            this.tblDoc.Controls.Add(this.label7, 0, 0);
            this.tblDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblDoc.Location = new System.Drawing.Point(3, 63);
            this.tblDoc.Name = "tblDoc";
            this.tblDoc.RowCount = 1;
            this.tblDoc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblDoc.Size = new System.Drawing.Size(458, 32);
            this.tblDoc.TabIndex = 0;
            // 
            // tbDocID
            // 
            this.tbDocID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDocID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbDocID.Location = new System.Drawing.Point(94, 3);
            this.tbDocID.MaxLength = 13;
            this.tbDocID.Name = "tbDocID";
            this.tbDocID.ReadOnly = true;
            this.tbDocID.Size = new System.Drawing.Size(361, 29);
            this.tbDocID.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 32);
            this.label7.TabIndex = 0;
            this.label7.Text = "เลขที่เอกสาร";
            // 
            // tblUserNameHeader
            // 
            this.tblUserNameHeader.ColumnCount = 2;
            this.tblUserNameHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82F));
            this.tblUserNameHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tblUserNameHeader.Controls.Add(this.textUserName, 0, 0);
            this.tblUserNameHeader.Controls.Add(this.pictFormName, 0, 0);
            this.tblUserNameHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblUserNameHeader.Location = new System.Drawing.Point(3, 3);
            this.tblUserNameHeader.Name = "tblUserNameHeader";
            this.tblUserNameHeader.RowCount = 1;
            this.tblUserNameHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblUserNameHeader.Size = new System.Drawing.Size(458, 54);
            this.tblUserNameHeader.TabIndex = 1;
            // 
            // textUserName
            // 
            this.textUserName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.textUserName.AutoSize = true;
            this.textUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textUserName.Location = new System.Drawing.Point(383, 2);
            this.textUserName.Name = "textUserName";
            this.textUserName.Size = new System.Drawing.Size(72, 50);
            this.textUserName.TabIndex = 11;
            this.textUserName.Text = "UserName";
            // 
            // pictFormName
            // 
            this.pictFormName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictFormName.Image = global::BookStore.Properties.Resources.คิดเงิน;
            this.pictFormName.Location = new System.Drawing.Point(3, 3);
            this.pictFormName.Name = "pictFormName";
            this.pictFormName.Size = new System.Drawing.Size(369, 48);
            this.pictFormName.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictFormName.TabIndex = 2;
            this.pictFormName.TabStop = false;
            // 
            // tblBody
            // 
            this.tblBody.ColumnCount = 2;
            this.tblBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tblBody.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tblBody.Controls.Add(this.dgvSales, 0, 0);
            this.tblBody.Controls.Add(this.tblInputMain, 1, 0);
            this.tblBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblBody.Location = new System.Drawing.Point(3, 113);
            this.tblBody.Name = "tblBody";
            this.tblBody.RowCount = 1;
            this.tblBody.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblBody.Size = new System.Drawing.Size(938, 314);
            this.tblBody.TabIndex = 1;
            // 
            // dgvSales
            // 
            this.dgvSales.AllowUserToAddRows = false;
            this.dgvSales.AllowUserToDeleteRows = false;
            this.dgvSales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSales.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.dgvSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ISBN,
            this.BookName,
            this.Quantity,
            this.Cost,
            this.Total});
            this.dgvSales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSales.Location = new System.Drawing.Point(3, 3);
            this.dgvSales.Name = "dgvSales";
            this.dgvSales.ReadOnly = true;
            this.dgvSales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSales.Size = new System.Drawing.Size(556, 308);
            this.dgvSales.TabIndex = 0;
            // 
            // ISBN
            // 
            this.ISBN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ISBN.HeaderText = "ISBN";
            this.ISBN.Name = "ISBN";
            this.ISBN.ReadOnly = true;
            // 
            // BookName
            // 
            this.BookName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BookName.HeaderText = "ชื่อหนังสือ";
            this.BookName.Name = "BookName";
            this.BookName.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Quantity.HeaderText = "จำนวน";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Cost
            // 
            this.Cost.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Cost.HeaderText = "ราคา";
            this.Cost.Name = "Cost";
            this.Cost.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Total.HeaderText = "ราคารวม";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // tblInputMain
            // 
            this.tblInputMain.ColumnCount = 1;
            this.tblInputMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblInputMain.Controls.Add(this.tblInput, 0, 0);
            this.tblInputMain.Controls.Add(this.tblButtonInput, 0, 1);
            this.tblInputMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblInputMain.Location = new System.Drawing.Point(565, 3);
            this.tblInputMain.Name = "tblInputMain";
            this.tblInputMain.RowCount = 2;
            this.tblInputMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82F));
            this.tblInputMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tblInputMain.Size = new System.Drawing.Size(370, 308);
            this.tblInputMain.TabIndex = 1;
            // 
            // tblInput
            // 
            this.tblInput.ColumnCount = 2;
            this.tblInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tblInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tblInput.Controls.Add(this.textGetM, 0, 3);
            this.tblInput.Controls.Add(this.textQuantity, 0, 2);
            this.tblInput.Controls.Add(this.textProductID, 0, 1);
            this.tblInput.Controls.Add(this.tbCusID, 1, 0);
            this.tblInput.Controls.Add(this.tbProductID, 1, 1);
            this.tblInput.Controls.Add(this.tbGetMoney, 1, 3);
            this.tblInput.Controls.Add(this.textCusID, 0, 0);
            this.tblInput.Controls.Add(this.nupQty, 1, 2);
            this.tblInput.Controls.Add(this.textDiscount, 1, 4);
            this.tblInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblInput.Location = new System.Drawing.Point(3, 3);
            this.tblInput.Name = "tblInput";
            this.tblInput.RowCount = 5;
            this.tblInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tblInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tblInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tblInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29F));
            this.tblInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tblInput.Size = new System.Drawing.Size(364, 246);
            this.tblInput.TabIndex = 3;
            // 
            // textGetM
            // 
            this.textGetM.AutoSize = true;
            this.textGetM.Dock = System.Windows.Forms.DockStyle.Right;
            this.textGetM.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textGetM.Location = new System.Drawing.Point(75, 138);
            this.textGetM.Name = "textGetM";
            this.textGetM.Size = new System.Drawing.Size(67, 71);
            this.textGetM.TabIndex = 7;
            this.textGetM.Text = "รับเงิน";
            // 
            // textQuantity
            // 
            this.textQuantity.AutoSize = true;
            this.textQuantity.Dock = System.Windows.Forms.DockStyle.Right;
            this.textQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textQuantity.Location = new System.Drawing.Point(70, 92);
            this.textQuantity.Name = "textQuantity";
            this.textQuantity.Size = new System.Drawing.Size(72, 46);
            this.textQuantity.TabIndex = 6;
            this.textQuantity.Text = "จำนวน";
            // 
            // textProductID
            // 
            this.textProductID.AutoSize = true;
            this.textProductID.Dock = System.Windows.Forms.DockStyle.Right;
            this.textProductID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textProductID.Location = new System.Drawing.Point(44, 46);
            this.textProductID.Name = "textProductID";
            this.textProductID.Size = new System.Drawing.Size(98, 46);
            this.textProductID.TabIndex = 5;
            this.textProductID.Text = "รหัสสินค้า";
            // 
            // tbCusID
            // 
            this.tbCusID.Dock = System.Windows.Forms.DockStyle.Left;
            this.tbCusID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbCusID.Location = new System.Drawing.Point(148, 3);
            this.tbCusID.MaxLength = 13;
            this.tbCusID.Name = "tbCusID";
            this.tbCusID.Size = new System.Drawing.Size(165, 31);
            this.tbCusID.TabIndex = 0;
            this.tbCusID.TextChanged += new System.EventHandler(this.txchinputCusID);
            this.tbCusID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.kdownCheckCusIDtoSave);
            this.tbCusID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber);
            // 
            // tbProductID
            // 
            this.tbProductID.Dock = System.Windows.Forms.DockStyle.Left;
            this.tbProductID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbProductID.Location = new System.Drawing.Point(148, 49);
            this.tbProductID.MaxLength = 13;
            this.tbProductID.Name = "tbProductID";
            this.tbProductID.Size = new System.Drawing.Size(165, 31);
            this.tbProductID.TabIndex = 1;
            this.tbProductID.TextChanged += new System.EventHandler(this.txchinputProductID);
            this.tbProductID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.kdownEnterAddProduct);
            this.tbProductID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber);
            // 
            // tbGetMoney
            // 
            this.tbGetMoney.Dock = System.Windows.Forms.DockStyle.Left;
            this.tbGetMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbGetMoney.Location = new System.Drawing.Point(148, 141);
            this.tbGetMoney.MaxLength = 12;
            this.tbGetMoney.Name = "tbGetMoney";
            this.tbGetMoney.Size = new System.Drawing.Size(165, 56);
            this.tbGetMoney.TabIndex = 3;
            this.tbGetMoney.TextChanged += new System.EventHandler(this.txchinputMoney);
            this.tbGetMoney.KeyDown += new System.Windows.Forms.KeyEventHandler(this.kdownEnterSave);
            this.tbGetMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber2);
            // 
            // textCusID
            // 
            this.textCusID.AutoSize = true;
            this.textCusID.Dock = System.Windows.Forms.DockStyle.Right;
            this.textCusID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textCusID.Location = new System.Drawing.Point(47, 0);
            this.textCusID.Name = "textCusID";
            this.textCusID.Size = new System.Drawing.Size(95, 46);
            this.textCusID.TabIndex = 4;
            this.textCusID.Text = "รหัสลูกค้า";
            // 
            // nupQty
            // 
            this.nupQty.Dock = System.Windows.Forms.DockStyle.Left;
            this.nupQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.nupQty.Location = new System.Drawing.Point(148, 95);
            this.nupQty.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.nupQty.Name = "nupQty";
            this.nupQty.Size = new System.Drawing.Size(165, 31);
            this.nupQty.TabIndex = 8;
            this.nupQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.kdownEnterAddProduct);
            this.nupQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber);
            // 
            // textDiscount
            // 
            this.textDiscount.AutoSize = true;
            this.textDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textDiscount.Location = new System.Drawing.Point(148, 209);
            this.textDiscount.Name = "textDiscount";
            this.textDiscount.Size = new System.Drawing.Size(213, 37);
            this.textDiscount.TabIndex = 10;
            this.textDiscount.TextChanged += new System.EventHandler(this.textDisShow);
            // 
            // tblButtonInput
            // 
            this.tblButtonInput.ColumnCount = 3;
            this.tblButtonInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblButtonInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblButtonInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tblButtonInput.Controls.Add(this.pictAdd, 0, 0);
            this.tblButtonInput.Controls.Add(this.pictDelete, 1, 0);
            this.tblButtonInput.Controls.Add(this.pictCheckBill, 2, 0);
            this.tblButtonInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblButtonInput.Location = new System.Drawing.Point(3, 255);
            this.tblButtonInput.Name = "tblButtonInput";
            this.tblButtonInput.RowCount = 1;
            this.tblButtonInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblButtonInput.Size = new System.Drawing.Size(364, 50);
            this.tblButtonInput.TabIndex = 4;
            // 
            // pictAdd
            // 
            this.pictAdd.BackColor = System.Drawing.Color.LightGreen;
            this.pictAdd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictAdd.Image = global::BookStore.Properties.Resources.add_button_inside_black_circle;
            this.pictAdd.Location = new System.Drawing.Point(3, 3);
            this.pictAdd.Name = "pictAdd";
            this.pictAdd.Size = new System.Drawing.Size(103, 44);
            this.pictAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictAdd.TabIndex = 2;
            this.pictAdd.TabStop = false;
            this.toolTip1.SetToolTip(this.pictAdd, "เพิ่มรายการสินค้า");
            this.pictAdd.Click += new System.EventHandler(this.pictAdd_Click);
            this.pictAdd.MouseLeave += new System.EventHandler(this.mplAdd);
            this.pictAdd.MouseHover += new System.EventHandler(this.mpAdd);
            // 
            // pictDelete
            // 
            this.pictDelete.BackColor = System.Drawing.Color.LightCoral;
            this.pictDelete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictDelete.Image = global::BookStore.Properties.Resources.rounded_remove_button;
            this.pictDelete.Location = new System.Drawing.Point(112, 3);
            this.pictDelete.Name = "pictDelete";
            this.pictDelete.Size = new System.Drawing.Size(103, 44);
            this.pictDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictDelete.TabIndex = 3;
            this.pictDelete.TabStop = false;
            this.toolTip1.SetToolTip(this.pictDelete, "ลบรายการสินค้า");
            this.pictDelete.Click += new System.EventHandler(this.pictDelete_Click);
            this.pictDelete.MouseLeave += new System.EventHandler(this.mplDel);
            this.pictDelete.MouseHover += new System.EventHandler(this.mpDel);
            // 
            // pictCheckBill
            // 
            this.pictCheckBill.BackColor = System.Drawing.Color.LightYellow;
            this.pictCheckBill.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictCheckBill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictCheckBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictCheckBill.Image = global::BookStore.Properties.Resources.receipt;
            this.pictCheckBill.Location = new System.Drawing.Point(221, 3);
            this.pictCheckBill.Name = "pictCheckBill";
            this.pictCheckBill.Size = new System.Drawing.Size(140, 44);
            this.pictCheckBill.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictCheckBill.TabIndex = 4;
            this.pictCheckBill.TabStop = false;
            this.toolTip1.SetToolTip(this.pictCheckBill, "ออกใบเสร็จ (คิดเงินบันทึกข้อมูล)");
            this.pictCheckBill.Click += new System.EventHandler(this.pictCheckBill_Click);
            this.pictCheckBill.MouseLeave += new System.EventHandler(this.mplCheckBill);
            this.pictCheckBill.MouseHover += new System.EventHandler(this.mpCheckBill);
            // 
            // tblFooter
            // 
            this.tblFooter.ColumnCount = 6;
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tblFooter.Controls.Add(this.TextTotal, 0, 0);
            this.tblFooter.Controls.Add(this.tbReturnMoney, 3, 0);
            this.tblFooter.Controls.Add(this.pictReport, 4, 0);
            this.tblFooter.Controls.Add(this.pictHome, 5, 0);
            this.tblFooter.Controls.Add(this.textReM, 2, 0);
            this.tblFooter.Controls.Add(this.tbTotal, 1, 0);
            this.tblFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblFooter.Location = new System.Drawing.Point(3, 433);
            this.tblFooter.Name = "tblFooter";
            this.tblFooter.RowCount = 1;
            this.tblFooter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblFooter.Size = new System.Drawing.Size(938, 65);
            this.tblFooter.TabIndex = 2;
            // 
            // TextTotal
            // 
            this.TextTotal.AutoSize = true;
            this.TextTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TextTotal.Location = new System.Drawing.Point(3, 0);
            this.TextTotal.Name = "TextTotal";
            this.TextTotal.Size = new System.Drawing.Size(78, 65);
            this.TextTotal.TabIndex = 15;
            this.TextTotal.Text = "ราคารวม";
            // 
            // tbReturnMoney
            // 
            this.tbReturnMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbReturnMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbReturnMoney.Location = new System.Drawing.Point(471, 3);
            this.tbReturnMoney.Name = "tbReturnMoney";
            this.tbReturnMoney.ReadOnly = true;
            this.tbReturnMoney.Size = new System.Drawing.Size(294, 56);
            this.tbReturnMoney.TabIndex = 9;
            // 
            // pictReport
            // 
            this.pictReport.BackColor = System.Drawing.Color.DarkGray;
            this.pictReport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictReport.Image = global::BookStore.Properties.Resources.chart;
            this.pictReport.Location = new System.Drawing.Point(771, 3);
            this.pictReport.Name = "pictReport";
            this.pictReport.Size = new System.Drawing.Size(78, 59);
            this.pictReport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictReport.TabIndex = 10;
            this.pictReport.TabStop = false;
            this.toolTip1.SetToolTip(this.pictReport, "รายงานแสดงรายการขาย");
            this.pictReport.Click += new System.EventHandler(this.pictReport_Click);
            this.pictReport.MouseLeave += new System.EventHandler(this.mplReport);
            this.pictReport.MouseHover += new System.EventHandler(this.mpReport);
            // 
            // pictHome
            // 
            this.pictHome.BackColor = System.Drawing.Color.DarkGray;
            this.pictHome.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictHome.Image = global::BookStore.Properties.Resources.house;
            this.pictHome.Location = new System.Drawing.Point(855, 3);
            this.pictHome.Name = "pictHome";
            this.pictHome.Size = new System.Drawing.Size(80, 59);
            this.pictHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictHome.TabIndex = 11;
            this.pictHome.TabStop = false;
            this.toolTip1.SetToolTip(this.pictHome, "กลับหน้าหลัก (เมนู)");
            this.pictHome.Click += new System.EventHandler(this.pictHome_Click);
            this.pictHome.MouseLeave += new System.EventHandler(this.mplHome);
            this.pictHome.MouseHover += new System.EventHandler(this.mpHome);
            // 
            // textReM
            // 
            this.textReM.AutoSize = true;
            this.textReM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textReM.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textReM.Location = new System.Drawing.Point(387, 0);
            this.textReM.Name = "textReM";
            this.textReM.Size = new System.Drawing.Size(78, 65);
            this.textReM.TabIndex = 13;
            this.textReM.Text = "เงินทอน";
            // 
            // tbTotal
            // 
            this.tbTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbTotal.Location = new System.Drawing.Point(87, 3);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.ReadOnly = true;
            this.tbTotal.Size = new System.Drawing.Size(294, 56);
            this.tbTotal.TabIndex = 14;
            this.tbTotal.TextChanged += new System.EventHandler(this.txchTotalM);
            // 
            // Sales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.tlpMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(768, 432);
            this.Name = "Sales";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmClosing);
            this.Load += new System.EventHandler(this.Sales_Load);
            this.tlpMain.ResumeLayout(false);
            this.tlpHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictNameShop)).EndInit();
            this.tblDocHeader.ResumeLayout(false);
            this.tblDoc.ResumeLayout(false);
            this.tblDoc.PerformLayout();
            this.tblUserNameHeader.ResumeLayout(false);
            this.tblUserNameHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictFormName)).EndInit();
            this.tblBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSales)).EndInit();
            this.tblInputMain.ResumeLayout(false);
            this.tblInput.ResumeLayout(false);
            this.tblInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupQty)).EndInit();
            this.tblButtonInput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictCheckBill)).EndInit();
            this.tblFooter.ResumeLayout(false);
            this.tblFooter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictHome)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpHeader;
        private System.Windows.Forms.PictureBox pictLogo;
        private System.Windows.Forms.PictureBox pictNameShop;
        private System.Windows.Forms.TableLayoutPanel tblBody;
        private System.Windows.Forms.DataGridView dgvSales;
        private System.Windows.Forms.TableLayoutPanel tblFooter;
        private System.Windows.Forms.TextBox tbReturnMoney;
        private System.Windows.Forms.PictureBox pictReport;
        private System.Windows.Forms.PictureBox pictHome;
        private System.Windows.Forms.TableLayoutPanel tblDocHeader;
        private System.Windows.Forms.TableLayoutPanel tblDoc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn ISBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Label textReM;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label TextTotal;
        private System.Windows.Forms.TableLayoutPanel tblInputMain;
        private System.Windows.Forms.TableLayoutPanel tblInput;
        private System.Windows.Forms.Label textGetM;
        private System.Windows.Forms.Label textQuantity;
        private System.Windows.Forms.Label textProductID;
        private System.Windows.Forms.TextBox tbCusID;
        private System.Windows.Forms.TextBox tbProductID;
        private System.Windows.Forms.TextBox tbGetMoney;
        private System.Windows.Forms.Label textCusID;
        private System.Windows.Forms.NumericUpDown nupQty;
        private System.Windows.Forms.TableLayoutPanel tblButtonInput;
        private System.Windows.Forms.TableLayoutPanel tblUserNameHeader;
        public System.Windows.Forms.Label textUserName;
        private System.Windows.Forms.PictureBox pictFormName;
        public System.Windows.Forms.TextBox tbDocID;
        private System.Windows.Forms.PictureBox pictAdd;
        private System.Windows.Forms.PictureBox pictDelete;
        private System.Windows.Forms.PictureBox pictCheckBill;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label textDiscount;
    }
}