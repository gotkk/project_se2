namespace BookStore
{
    partial class ReportData
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportData));
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.Group group2 = new Telerik.Reporting.Group();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.saleIDGroupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.saleIDGroupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.iSBNCaptionTextBox = new Telerik.Reporting.TextBox();
            this.bookNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.quantityCaptionTextBox = new Telerik.Reporting.TextBox();
            this.costCaptionTextBox = new Telerik.Reporting.TextBox();
            this.totalCostCaptionTextBox = new Telerik.Reporting.TextBox();
            this.saleIDCaptionTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.titleTextBox = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.reportFooter = new Telerik.Reporting.ReportFooterSection();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.iSBNDataTextBox = new Telerik.Reporting.TextBox();
            this.bookNameDataTextBox = new Telerik.Reporting.TextBox();
            this.quantityDataTextBox = new Telerik.Reporting.TextBox();
            this.costDataTextBox = new Telerik.Reporting.TextBox();
            this.totalCostDataTextBox = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupFooterSection
            // 
            this.labelsGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.49999997019767761D);
            this.labelsGroupFooterSection.Name = "labelsGroupFooterSection";
            this.labelsGroupFooterSection.Style.Visible = false;
            // 
            // labelsGroupHeaderSection
            // 
            this.labelsGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000007748603821D);
            this.labelsGroupHeaderSection.Name = "labelsGroupHeaderSection";
            this.labelsGroupHeaderSection.PrintOnEveryPage = true;
            // 
            // saleIDGroupFooterSection
            // 
            this.saleIDGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(2.5499014854431152D);
            this.saleIDGroupFooterSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape1,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox15,
            this.textBox14});
            this.saleIDGroupFooterSection.Name = "saleIDGroupFooterSection";
            // 
            // textBox10
            // 
            this.textBox10.CanGrow = true;
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2000398635864258D), Telerik.Reporting.Drawing.Unit.Inch(0.64974379539489746D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0341243743896484D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox10.Style.Color = System.Drawing.Color.Black;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.StyleName = "Caption";
            this.textBox10.Value = "�Ѻ�Թ";
            // 
            // textBox11
            // 
            this.textBox11.CanGrow = true;
            this.textBox11.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox11.Format = "{0:N2}";
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2759099006652832D), Telerik.Reporting.Drawing.Unit.Inch(0.643443763256073D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9709742069244385D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox11.StyleName = "Data";
            this.textBox11.Value = "= Fields.GetMoney";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = true;
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.25378164649009705D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0341236591339111D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox12.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox12.Style.Color = System.Drawing.Color.Black;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.StyleName = "Caption";
            this.textBox12.Value = "�Ҥ����������(��ѧ�ѡ��ǹŴ)";
            // 
            // textBox13
            // 
            this.textBox13.CanGrow = true;
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(1.0498224496841431D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0341243743896484D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox13.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox13.Style.Color = System.Drawing.Color.Black;
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.StyleName = "Caption";
            this.textBox13.Value = "�Թ�͹";
            // 
            // textBox14
            // 
            this.textBox14.CanGrow = true;
            this.textBox14.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox14.Format = "{0:N2}";
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2342433929443359D), Telerik.Reporting.Drawing.Unit.Inch(0.25378164649009705D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9709742069244385D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.StyleName = "Data";
            this.textBox14.Value = "= Fields.Total";
            // 
            // textBox15
            // 
            this.textBox15.CanGrow = true;
            this.textBox15.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox15.Format = "{0:N2}";
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2342028617858887D), Telerik.Reporting.Drawing.Unit.Inch(1.0498224496841431D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9709742069244385D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox15.StyleName = "Data";
            this.textBox15.Value = "= Fields.ReturnMoney";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(2.1137893199920654D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.1843838691711426D), Telerik.Reporting.Drawing.Unit.Inch(0.19999991357326508D));
            // 
            // saleIDGroupHeaderSection
            // 
            this.saleIDGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(2.0084319114685059D);
            this.saleIDGroupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.iSBNCaptionTextBox,
            this.bookNameCaptionTextBox,
            this.quantityCaptionTextBox,
            this.costCaptionTextBox,
            this.totalCostCaptionTextBox,
            this.saleIDCaptionTextBox,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox17,
            this.textBox16});
            this.saleIDGroupHeaderSection.Name = "saleIDGroupHeaderSection";
            // 
            // iSBNCaptionTextBox
            // 
            this.iSBNCaptionTextBox.CanGrow = true;
            this.iSBNCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(1.508432149887085D));
            this.iSBNCaptionTextBox.Name = "iSBNCaptionTextBox";
            this.iSBNCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133694410324097D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.iSBNCaptionTextBox.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(58)))), ((int)(((byte)(112)))));
            this.iSBNCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.iSBNCaptionTextBox.StyleName = "Caption";
            this.iSBNCaptionTextBox.Value = "����˹ѧ���";
            // 
            // bookNameCaptionTextBox
            // 
            this.bookNameCaptionTextBox.CanGrow = true;
            this.bookNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0342816114425659D), Telerik.Reporting.Drawing.Unit.Inch(1.508432149887085D));
            this.bookNameCaptionTextBox.Name = "bookNameCaptionTextBox";
            this.bookNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0682864189147949D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.bookNameCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.bookNameCaptionTextBox.StyleName = "Caption";
            this.bookNameCaptionTextBox.Value = "����˹ѧ���";
            // 
            // quantityCaptionTextBox
            // 
            this.quantityCaptionTextBox.CanGrow = true;
            this.quantityCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1026468276977539D), Telerik.Reporting.Drawing.Unit.Inch(1.508432149887085D));
            this.quantityCaptionTextBox.Name = "quantityCaptionTextBox";
            this.quantityCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133694410324097D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.quantityCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.quantityCaptionTextBox.StyleName = "Caption";
            this.quantityCaptionTextBox.Value = "�ӹǹ";
            // 
            // costCaptionTextBox
            // 
            this.costCaptionTextBox.CanGrow = true;
            this.costCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1160945892333984D), Telerik.Reporting.Drawing.Unit.Inch(1.508432149887085D));
            this.costCaptionTextBox.Name = "costCaptionTextBox";
            this.costCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0341238975524902D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.costCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.costCaptionTextBox.StyleName = "Caption";
            this.costCaptionTextBox.Value = "�Ҥ�";
            // 
            // totalCostCaptionTextBox
            // 
            this.totalCostCaptionTextBox.CanGrow = true;
            this.totalCostCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1502971649169922D), Telerik.Reporting.Drawing.Unit.Inch(1.508432149887085D));
            this.totalCostCaptionTextBox.Name = "totalCostCaptionTextBox";
            this.totalCostCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0549191236495972D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.totalCostCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.totalCostCaptionTextBox.StyleName = "Caption";
            this.totalCostCaptionTextBox.Value = "�Ҥ����";
            // 
            // saleIDCaptionTextBox
            // 
            this.saleIDCaptionTextBox.CanGrow = true;
            this.saleIDCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020833544433116913D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.saleIDCaptionTextBox.Name = "saleIDCaptionTextBox";
            this.saleIDCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133694410324097D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.saleIDCaptionTextBox.Style.BackgroundColor = System.Drawing.Color.White;
            this.saleIDCaptionTextBox.Style.Color = System.Drawing.Color.Black;
            this.saleIDCaptionTextBox.Style.Font.Bold = true;
            this.saleIDCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.saleIDCaptionTextBox.StyleName = "Caption";
            this.saleIDCaptionTextBox.Value = "�Ţ����͡���";
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = true;
            this.textBox1.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0550360679626465D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2449638843536377D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox1.StyleName = "Data";
            this.textBox1.Value = "= Fields.SaleID";
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = true;
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0416668653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.50843197107315063D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.858333170413971D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox2.StyleName = "Data";
            this.textBox2.Value = "= Fields.CTitle";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = true;
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020833544433116913D), Telerik.Reporting.Drawing.Unit.Inch(0.50843197107315063D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133694410324097D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox3.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox3.Style.Color = System.Drawing.Color.Black;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.StyleName = "Caption";
            this.textBox3.Value = "�����١���";
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = true;
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.51254844665527344D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1684842109680176D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox4.StyleName = "Data";
            this.textBox4.Value = "= Fields.CFirstName";
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = true;
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0685629844665527D), Telerik.Reporting.Drawing.Unit.Inch(0.51254844665527344D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2314369678497315D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox5.StyleName = "Data";
            this.textBox5.Value = "= Fields.CLastName";
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = true;
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0729169845581055D), Telerik.Reporting.Drawing.Unit.Inch(1.0084319114685059D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2270827293395996D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox6.StyleName = "Data";
            this.textBox6.Value = "= Fields.ULastName";
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = true;
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(1.0084319114685059D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1654528379440308D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox7.StyleName = "Data";
            this.textBox7.Value = "= Fields.UFirstName";
            // 
            // textBox8
            // 
            this.textBox8.CanGrow = true;
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020833544433116913D), Telerik.Reporting.Drawing.Unit.Inch(1.0084319114685059D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133694410324097D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox8.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox8.Style.Color = System.Drawing.Color.Black;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.StyleName = "Caption";
            this.textBox8.Value = "���ͼ����";
            // 
            // textBox9
            // 
            this.textBox9.CanGrow = true;
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0416668653488159D), Telerik.Reporting.Drawing.Unit.Inch(1.0084319114685059D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.858333170413971D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox9.StyleName = "Data";
            this.textBox9.Value = "= Fields.UTitle";
            // 
            // textBox17
            // 
            this.textBox17.CanGrow = true;
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1368503570556641D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.36314964294433594D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox17.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox17.Style.Color = System.Drawing.Color.Black;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.StyleName = "Caption";
            this.textBox17.Value = "�ѹ���";
            // 
            // textBox16
            // 
            this.textBox16.CanGrow = true;
            this.textBox16.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5000786781311035D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6813907623291016D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.StyleName = "Data";
            this.textBox16.Value = "= Fields.Date";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "Data Source=dbshop.database.windows.net;Initial Catalog=BookShop;User ID=BookShop" +
    ";Password=Shop1234";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.ProviderName = "System.Data.SqlClient";
            this.sqlDataSource1.SelectCommand = resources.GetString("sqlDataSource1.SelectCommand");
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.10000000894069672D);
            this.pageHeader.Name = "pageHeader";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.74166738986968994D);
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.titleTextBox,
            this.shape2});
            this.reportHeader.Name = "reportHeader";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2051773071289062D), Telerik.Reporting.Drawing.Unit.Inch(0.59999996423721313D));
            this.titleTextBox.Style.Font.Bold = true;
            this.titleTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.titleTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.titleTextBox.StyleName = "Title";
            this.titleTextBox.Value = "��§ҹ��â��˹ѧ���";
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.70000004768371582D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.1843838691711426D), Telerik.Reporting.Drawing.Unit.Inch(0.27291679382324219D));
            // 
            // reportFooter
            // 
            this.reportFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D);
            this.reportFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox19,
            this.textBox22,
            this.currentTimeTextBox,
            this.textBox18,
            this.textBox20,
            this.textBox23});
            this.reportFooter.Name = "reportFooter";
            // 
            // textBox21
            // 
            this.textBox21.CanGrow = true;
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(1.2083345651626587D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2051773071289062D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox21.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox21.Style.Color = System.Drawing.Color.Black;
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox21.StyleName = "Caption";
            this.textBox21.Value = "��ػ�ʹ��·�����";
            // 
            // textBox19
            // 
            this.textBox19.CanGrow = true;
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(1.7833346128463745D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox19.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox19.Style.Color = System.Drawing.Color.Black;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox19.StyleName = "Caption";
            this.textBox19.Value = "�Ҥ�����ʹ��·�����";
            // 
            // textBox22
            // 
            this.textBox22.CanGrow = true;
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6260104179382324D), Telerik.Reporting.Drawing.Unit.Inch(1.783334493637085D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60000014305114746D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox22.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox22.Style.Color = System.Drawing.Color.Black;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.StyleName = "Caption";
            this.textBox22.Value = "�ҷ";
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4585309028625488D), Telerik.Reporting.Drawing.Unit.Inch(0.12499977648258209D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7258924245834351D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.currentTimeTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // textBox18
            // 
            this.textBox18.CanGrow = true;
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8376184701919556D), Telerik.Reporting.Drawing.Unit.Inch(0.12500001490116119D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6208336353302D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox18.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox18.Style.Color = System.Drawing.Color.Black;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.StyleName = "Caption";
            this.textBox18.Value = "�ѹ����͡��§ҹ";
            // 
            // textBox20
            // 
            this.textBox20.CanGrow = true;
            this.textBox20.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox20.Format = "{0:N2}";
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6446201801300049D), Telerik.Reporting.Drawing.Unit.Inch(1.7833346128463745D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.981311559677124D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox20.Style.Font.Underline = true;
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox20.StyleName = "Data";
            this.textBox20.Value = "= sum( Fields.Total)";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.44166669249534607D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.iSBNDataTextBox,
            this.bookNameDataTextBox,
            this.quantityDataTextBox,
            this.costDataTextBox,
            this.totalCostDataTextBox});
            this.detail.Name = "detail";
            // 
            // iSBNDataTextBox
            // 
            this.iSBNDataTextBox.CanGrow = true;
            this.iSBNDataTextBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.iSBNDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.iSBNDataTextBox.Name = "iSBNDataTextBox";
            this.iSBNDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133694410324097D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.iSBNDataTextBox.StyleName = "Data";
            this.iSBNDataTextBox.Value = "= Fields.ISBN";
            // 
            // bookNameDataTextBox
            // 
            this.bookNameDataTextBox.CanGrow = true;
            this.bookNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0416668653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.bookNameDataTextBox.Name = "bookNameDataTextBox";
            this.bookNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0609414577484131D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.bookNameDataTextBox.StyleName = "Data";
            this.bookNameDataTextBox.Value = "= Fields.BookName";
            // 
            // quantityDataTextBox
            // 
            this.quantityDataTextBox.CanGrow = true;
            this.quantityDataTextBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.quantityDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1026871204376221D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.quantityDataTextBox.Name = "quantityDataTextBox";
            this.quantityDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0133287906646729D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.quantityDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.quantityDataTextBox.StyleName = "Data";
            this.quantityDataTextBox.Value = "= Fields.Quantity";
            // 
            // costDataTextBox
            // 
            this.costDataTextBox.CanGrow = true;
            this.costDataTextBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.costDataTextBox.Format = "{0:N2}";
            this.costDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1160945892333984D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.costDataTextBox.Name = "costDataTextBox";
            this.costDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0341238975524902D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.costDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.costDataTextBox.StyleName = "Data";
            this.costDataTextBox.Value = "= Fields.Cost";
            // 
            // totalCostDataTextBox
            // 
            this.totalCostDataTextBox.CanGrow = true;
            this.totalCostDataTextBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.totalCostDataTextBox.Format = "{0:N2}";
            this.totalCostDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.15029764175415D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.totalCostDataTextBox.Name = "totalCostDataTextBox";
            this.totalCostDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0549191236495972D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.totalCostDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.totalCostDataTextBox.StyleName = "Data";
            this.totalCostDataTextBox.Value = "= Fields.TotalCost";
            // 
            // textBox23
            // 
            this.textBox23.CanGrow = true;
            this.textBox23.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox23.Format = "{0:N2}";
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(2.5D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.981311559677124D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox23.Style.Font.Underline = true;
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.StyleName = "Data";
            this.textBox23.Value = "= sum( Fields.Total)";
            // 
            // ReportData
            // 
            this.DataSource = this.sqlDataSource1;
            group1.GroupFooter = this.labelsGroupFooterSection;
            group1.GroupHeader = this.labelsGroupHeaderSection;
            group1.Name = "labelsGroup";
            group2.GroupFooter = this.saleIDGroupFooterSection;
            group2.GroupHeader = this.saleIDGroupHeaderSection;
            group2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.SaleID"));
            group2.Name = "saleIDGroup";
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1,
            group2});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeaderSection,
            this.labelsGroupFooterSection,
            this.saleIDGroupHeaderSection,
            this.saleIDGroupFooterSection,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.reportFooter,
            this.detail});
            this.Name = "ReportData";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(58)))), ((int)(((byte)(112)))));
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule3.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(58)))), ((int)(((byte)(112)))));
            styleRule3.Style.Color = System.Drawing.Color.White;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule4.Style.Color = System.Drawing.Color.Black;
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule5.Style.Color = System.Drawing.Color.Black;
            styleRule5.Style.Font.Name = "Tahoma";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.246884822845459D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.SqlDataSource sqlDataSource1;
        private Telerik.Reporting.GroupHeaderSection labelsGroupHeaderSection;
        private Telerik.Reporting.TextBox saleIDCaptionTextBox;
        private Telerik.Reporting.TextBox iSBNCaptionTextBox;
        private Telerik.Reporting.TextBox bookNameCaptionTextBox;
        private Telerik.Reporting.TextBox quantityCaptionTextBox;
        private Telerik.Reporting.TextBox costCaptionTextBox;
        private Telerik.Reporting.TextBox totalCostCaptionTextBox;
        private Telerik.Reporting.GroupFooterSection labelsGroupFooterSection;
        private Telerik.Reporting.GroupHeaderSection saleIDGroupHeaderSection;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.GroupFooterSection saleIDGroupFooterSection;
        private Telerik.Reporting.PageHeaderSection pageHeader;
        private Telerik.Reporting.PageFooterSection pageFooter;
        private Telerik.Reporting.ReportHeaderSection reportHeader;
        private Telerik.Reporting.TextBox titleTextBox;
        private Telerik.Reporting.ReportFooterSection reportFooter;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox iSBNDataTextBox;
        private Telerik.Reporting.TextBox bookNameDataTextBox;
        private Telerik.Reporting.TextBox quantityDataTextBox;
        private Telerik.Reporting.TextBox costDataTextBox;
        private Telerik.Reporting.TextBox totalCostDataTextBox;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox currentTimeTextBox;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox23;
    }
}