﻿using BookStore.Process;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class AddProduct : Form
    {
        public AddProduct()
        {
            InitializeComponent();
        }

        private void AddUser_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
        }

        private void pictOK_Click(object sender, EventArgs e)
        {
            checkDatatoAdd();
        }

        private void pictCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void checkDatatoAdd()
        {
            if (tbISBN.Text == "" || tbBookName.Text == "" || tbAuthor.Text == "" || tbPublisher.Text == "" || tbPrintYear.Text == "" || tbType.Text == ""||tbQuantity.Text ==""||tbCost.Text =="")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tbISBN.Text.Length < 13)
            {
                MessageBox.Show("กรุณากรอกรหัสหนังสือให้ครบ 13 หลัก", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbISBN.Focus();
                return;
            }

            //Check userID Used to in db
            DataTable checkdataISBN = ConnectionDB.executeSQL("SELECT * FROM Store WHERE ISBN='" + tbISBN.Text + "'");
            if (checkdataISBN.Rows.Count > 0)
            {
                MessageBox.Show("รหัสหนังสือนี้ถูกใช้ไปแล้ว กรุณากรอกรหัสหนังสือใหม่ให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbISBN.Clear();
                tbISBN.Focus();
                return;
            }

            ConnectionDB.executeSQL("INSERT INTO Store (ISBN,Name,Author,Publisher,PrintYear,Type,Quantity,Cost) VALUES('" + tbISBN.Text + "','" + tbBookName.Text + "','" + tbAuthor.Text + "','" + tbPublisher.Text + "','" + tbPrintYear.Text + "','" + tbType.Text + "','"+tbQuantity.Text+"','"+tbCost.Text+"')");
            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void pressNumber(object sender, KeyPressEventArgs e)
        {
            /* other
            if (e.Handled = !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }*/

   /*         if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13) )
            {
                e.Handled = true;
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }*/

            PCValueInput pc = new PCValueInput();
            pc.pressNumber(e);
        }

        private void pressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                checkDatatoAdd();
            }
        }

        //button zoom
        private void mpOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(60, 60);
        }

        private void mplOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(50, 50);
        }

        private void mpCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(60, 60);
        }

        private void mplCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(50, 50);
        }

        private void pressNumber2(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
            }

            //check if '.' , ',' pressed
            char sepratorChar = 's';
            if (e.KeyChar == '.')
            {
                // check if it's in the beginning of text not accept
                if (tbCost.Text.Length == 0)
                {
                    MessageBox.Show("ไม่สามารถกรอกข้อมูลขึ้นต้นด้วย (.) ได้ ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
                // check if it's in the beginning of text not accept
                if (tbCost.SelectionStart == 0)
                {
//                    MessageBox.Show("ไม่สามารถกรอกข้อมูลขึ้นต้นด้วย (.)2", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
                // check if there is already exist a '.' , ','
                if (alreadyExist(tbCost.Text, ref sepratorChar))
                {
                    MessageBox.Show("ไม่สามรถกรอกข้อมูล (.) ซ้ำได้ ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
                //check if '.' or ',' is in middle of a number and after it is not a number greater than 99
                if (tbCost.SelectionStart != tbCost.Text.Length && e.Handled == false)
                {
                    // '.' or ',' is in the middle
                    string AfterDotString = tbCost.Text.Substring(tbCost.SelectionStart);

                    if (AfterDotString.Length > 2)
                    {
//                        MessageBox.Show("ไม่สามารถกรอกข้อมูลตัวเลขทศนิยมเกิน 2 ตำแหน่ง1 ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Handled = true;
                    }
                }
            }
            //check if a number pressed

            if (Char.IsDigit(e.KeyChar))
            {
                //check if a coma or dot exist
                if (alreadyExist(tbCost.Text, ref sepratorChar))
                {
                    int sepratorPosition = tbCost.Text.IndexOf(sepratorChar);
                    string afterSepratorString = tbCost.Text.Substring(sepratorPosition + 1);
                    if (tbCost.SelectionStart > sepratorPosition && afterSepratorString.Length > 1)
                    {
                        MessageBox.Show("ไม่สามารถกรอกข้อมูลตัวเลขทศนิยมเกิน 2 ตำแหน่ง ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Handled = true;
                    }

                }
            }

       //     PCValueInput pc = new PCValueInput();
       //     pc.pressNumber2(sender,e);
        }


        private bool alreadyExist(string _text, ref char KeyChar)
        {
            if (_text.IndexOf('.') > -1)
            {
                KeyChar = '.';
                return true;
            }
            if (_text.IndexOf(',') > -1)
            {
                KeyChar = ',';
                return true;
            }
            return false;
        }
    }
}
