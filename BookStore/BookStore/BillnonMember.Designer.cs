namespace BookStore
{
    partial class BillnonMember
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillnonMember));
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.Group group2 = new Telerik.Reporting.Group();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule5 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.bookNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.quantityCaptionTextBox = new Telerik.Reporting.TextBox();
            this.totalCostCaptionTextBox = new Telerik.Reporting.TextBox();
            this.saleIDGroupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.totalDataTextBox = new Telerik.Reporting.TextBox();
            this.getMoneyDataTextBox = new Telerik.Reporting.TextBox();
            this.returnMoneyDataTextBox = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.returnMoneyCaptionTextBox = new Telerik.Reporting.TextBox();
            this.getMoneyCaptionTextBox = new Telerik.Reporting.TextBox();
            this.totalCaptionTextBox = new Telerik.Reporting.TextBox();
            this.saleIDGroupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.sqlDataSource1 = new Telerik.Reporting.SqlDataSource();
            this.saleIDCaptionTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.reportNameTextBox = new Telerik.Reporting.TextBox();
            this.dateDataTextBox = new Telerik.Reporting.TextBox();
            this.dateCaptionTextBox = new Telerik.Reporting.TextBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.expr1CaptionTextBox = new Telerik.Reporting.TextBox();
            this.expr1DataTextBox = new Telerik.Reporting.TextBox();
            this.expr2DataTextBox = new Telerik.Reporting.TextBox();
            this.expr3DataTextBox = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.reportFooter = new Telerik.Reporting.ReportFooterSection();
            this.detail = new Telerik.Reporting.DetailSection();
            this.quantityDataTextBox = new Telerik.Reporting.TextBox();
            this.totalCostDataTextBox = new Telerik.Reporting.TextBox();
            this.bookNameDataTextBox = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupFooterSection
            // 
            this.labelsGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.3945830762386322D);
            this.labelsGroupFooterSection.Name = "labelsGroupFooterSection";
            this.labelsGroupFooterSection.Style.Visible = false;
            // 
            // labelsGroupHeaderSection
            // 
            this.labelsGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.44166669249534607D);
            this.labelsGroupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.bookNameCaptionTextBox,
            this.quantityCaptionTextBox,
            this.totalCostCaptionTextBox});
            this.labelsGroupHeaderSection.Name = "labelsGroupHeaderSection";
            this.labelsGroupHeaderSection.PrintOnEveryPage = true;
            // 
            // bookNameCaptionTextBox
            // 
            this.bookNameCaptionTextBox.CanGrow = true;
            this.bookNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020833304151892662D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.bookNameCaptionTextBox.Name = "bookNameCaptionTextBox";
            this.bookNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4790878295898438D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.bookNameCaptionTextBox.Style.Font.Bold = true;
            this.bookNameCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.bookNameCaptionTextBox.StyleName = "Caption";
            this.bookNameCaptionTextBox.Value = "����˹ѧ���";
            // 
            // quantityCaptionTextBox
            // 
            this.quantityCaptionTextBox.CanGrow = true;
            this.quantityCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.quantityCaptionTextBox.Name = "quantityCaptionTextBox";
            this.quantityCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99054968357086182D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.quantityCaptionTextBox.Style.Font.Bold = true;
            this.quantityCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.quantityCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.quantityCaptionTextBox.StyleName = "Caption";
            this.quantityCaptionTextBox.Value = "�ӹǹ";
            // 
            // totalCostCaptionTextBox
            // 
            this.totalCostCaptionTextBox.CanGrow = true;
            this.totalCostCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4906282424926758D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.totalCostCaptionTextBox.Name = "totalCostCaptionTextBox";
            this.totalCostCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7770881652832031D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.totalCostCaptionTextBox.Style.Font.Bold = true;
            this.totalCostCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.totalCostCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.totalCostCaptionTextBox.StyleName = "Caption";
            this.totalCostCaptionTextBox.Value = "�Ҥ�";
            // 
            // saleIDGroupFooterSection
            // 
            this.saleIDGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(2.0699996948242188D);
            this.saleIDGroupFooterSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.totalDataTextBox,
            this.getMoneyDataTextBox,
            this.returnMoneyDataTextBox,
            this.shape2,
            this.returnMoneyCaptionTextBox,
            this.getMoneyCaptionTextBox,
            this.totalCaptionTextBox});
            this.saleIDGroupFooterSection.Name = "saleIDGroupFooterSection";
            // 
            // totalDataTextBox
            // 
            this.totalDataTextBox.CanGrow = true;
            this.totalDataTextBox.Format = "{0:N2}";
            this.totalDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3795979022979736D), Telerik.Reporting.Drawing.Unit.Inch(0.86458367109298706D));
            this.totalDataTextBox.Name = "totalDataTextBox";
            this.totalDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8840487003326416D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.totalDataTextBox.Style.Font.Bold = true;
            this.totalDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.totalDataTextBox.StyleName = "Data";
            this.totalDataTextBox.Value = "= Fields.Total";
            // 
            // getMoneyDataTextBox
            // 
            this.getMoneyDataTextBox.CanGrow = true;
            this.getMoneyDataTextBox.Format = "{0:N2}";
            this.getMoneyDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3756065368652344D), Telerik.Reporting.Drawing.Unit.Inch(1.2699995040893555D));
            this.getMoneyDataTextBox.Name = "getMoneyDataTextBox";
            this.getMoneyDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8841674327850342D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.getMoneyDataTextBox.Style.Font.Bold = true;
            this.getMoneyDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.getMoneyDataTextBox.StyleName = "Data";
            this.getMoneyDataTextBox.Value = "= Fields.GetMoney";
            // 
            // returnMoneyDataTextBox
            // 
            this.returnMoneyDataTextBox.CanGrow = true;
            this.returnMoneyDataTextBox.Format = "{0:N2}";
            this.returnMoneyDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3756065368652344D), Telerik.Reporting.Drawing.Unit.Inch(1.6699997186660767D));
            this.returnMoneyDataTextBox.Name = "returnMoneyDataTextBox";
            this.returnMoneyDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8840887546539307D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.returnMoneyDataTextBox.Style.Font.Bold = true;
            this.returnMoneyDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.returnMoneyDataTextBox.StyleName = "Data";
            this.returnMoneyDataTextBox.Value = "= Fields.ReturnMoney";
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.26458296179771423D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2508354187011719D), Telerik.Reporting.Drawing.Unit.Inch(0.27291679382324219D));
            // 
            // returnMoneyCaptionTextBox
            // 
            this.returnMoneyCaptionTextBox.CanGrow = true;
            this.returnMoneyCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.6625505685806274D));
            this.returnMoneyCaptionTextBox.Name = "returnMoneyCaptionTextBox";
            this.returnMoneyCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3790483474731445D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.returnMoneyCaptionTextBox.Style.Font.Bold = true;
            this.returnMoneyCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.returnMoneyCaptionTextBox.StyleName = "Caption";
            this.returnMoneyCaptionTextBox.Value = "�Թ�͹";
            // 
            // getMoneyCaptionTextBox
            // 
            this.getMoneyCaptionTextBox.CanGrow = true;
            this.getMoneyCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.2667173147201538D));
            this.getMoneyCaptionTextBox.Name = "getMoneyCaptionTextBox";
            this.getMoneyCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.37951922416687D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.getMoneyCaptionTextBox.Style.Font.Bold = true;
            this.getMoneyCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.getMoneyCaptionTextBox.StyleName = "Caption";
            this.getMoneyCaptionTextBox.Value = "�Ѻ�Թ";
            // 
            // totalCaptionTextBox
            // 
            this.totalCaptionTextBox.CanGrow = true;
            this.totalCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.860467255115509D));
            this.totalCaptionTextBox.Name = "totalCaptionTextBox";
            this.totalCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.37951922416687D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.totalCaptionTextBox.Style.Font.Bold = true;
            this.totalCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.totalCaptionTextBox.StyleName = "Caption";
            this.totalCaptionTextBox.Value = "�Ҥ����������";
            // 
            // saleIDGroupHeaderSection
            // 
            this.saleIDGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D);
            this.saleIDGroupHeaderSection.Name = "saleIDGroupHeaderSection";
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionString = "Data Source=dbshop.database.windows.net;Initial Catalog=BookStore;User ID=BookSho" +
    "p;Password=Shop1234";
            this.sqlDataSource1.Name = "sqlDataSource1";
            this.sqlDataSource1.ProviderName = "System.Data.SqlClient";
            this.sqlDataSource1.SelectCommand = resources.GetString("sqlDataSource1.SelectCommand");
            // 
            // saleIDCaptionTextBox
            // 
            this.saleIDCaptionTextBox.CanGrow = true;
            this.saleIDCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.104048490524292D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.saleIDCaptionTextBox.Name = "saleIDCaptionTextBox";
            this.saleIDCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6535507440567017D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.saleIDCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.saleIDCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.saleIDCaptionTextBox.StyleName = "Caption";
            this.saleIDCaptionTextBox.Value = "�Ţ��������";
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = true;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7675986289978027D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5000007152557373D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox1.StyleName = "Data";
            this.textBox1.Value = "= Fields.SaleID";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.84986060857772827D);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox,
            this.dateDataTextBox,
            this.dateCaptionTextBox});
            this.pageHeader.Name = "pageHeader";
            // 
            // reportNameTextBox
            // 
            this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.reportNameTextBox.Name = "reportNameTextBox";
            this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0831360816955566D), Telerik.Reporting.Drawing.Unit.Inch(0.82902723550796509D));
            this.reportNameTextBox.Style.Font.Bold = true;
            this.reportNameTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(32D);
            this.reportNameTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.reportNameTextBox.StyleName = "PageInfo";
            this.reportNameTextBox.Value = "BookShop";
            // 
            // dateDataTextBox
            // 
            this.dateDataTextBox.CanGrow = true;
            this.dateDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.674746036529541D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.dateDataTextBox.Name = "dateDataTextBox";
            this.dateDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5929704904556274D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.dateDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.dateDataTextBox.StyleName = "Data";
            this.dateDataTextBox.Value = "= Fields.Date";
            // 
            // dateCaptionTextBox
            // 
            this.dateCaptionTextBox.CanGrow = true;
            this.dateCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.104048490524292D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.dateCaptionTextBox.Name = "dateCaptionTextBox";
            this.dateCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5498640537261963D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.dateCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.dateCaptionTextBox.StyleName = "Caption";
            this.dateCaptionTextBox.Value = "Date:";
            // 
            // reportHeader
            // 
            this.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(1.7501399517059326D);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.expr1CaptionTextBox,
            this.expr1DataTextBox,
            this.expr2DataTextBox,
            this.expr3DataTextBox,
            this.saleIDCaptionTextBox,
            this.textBox1,
            this.shape1});
            this.reportHeader.Name = "reportHeader";
            // 
            // expr1CaptionTextBox
            // 
            this.expr1CaptionTextBox.CanGrow = true;
            this.expr1CaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0594589710235596D), Telerik.Reporting.Drawing.Unit.Inch(0.74602299928665161D));
            this.expr1CaptionTextBox.Name = "expr1CaptionTextBox";
            this.expr1CaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.expr1CaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.expr1CaptionTextBox.StyleName = "Caption";
            this.expr1CaptionTextBox.Value = "����͡��� : ";
            // 
            // expr1DataTextBox
            // 
            this.expr1DataTextBox.CanGrow = true;
            this.expr1DataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0595378875732422D), Telerik.Reporting.Drawing.Unit.Inch(0.75013953447341919D));
            this.expr1DataTextBox.Name = "expr1DataTextBox";
            this.expr1DataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.expr1DataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.expr1DataTextBox.StyleName = "Data";
            this.expr1DataTextBox.Value = "= Fields.Title";
            // 
            // expr2DataTextBox
            // 
            this.expr2DataTextBox.CanGrow = true;
            this.expr2DataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8596169948577881D), Telerik.Reporting.Drawing.Unit.Inch(0.75013953447341919D));
            this.expr2DataTextBox.Name = "expr2DataTextBox";
            this.expr2DataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.expr2DataTextBox.StyleName = "Data";
            this.expr2DataTextBox.Value = "= Fields.FirstName";
            // 
            // expr3DataTextBox
            // 
            this.expr3DataTextBox.CanGrow = true;
            this.expr3DataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.0596957206726074D), Telerik.Reporting.Drawing.Unit.Inch(0.75013953447341919D));
            this.expr3DataTextBox.Name = "expr3DataTextBox";
            this.expr3DataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.expr3DataTextBox.StyleName = "Data";
            this.expr3DataTextBox.Value = "= Fields.LastName";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.3501396179199219D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.2989273071289062D), Telerik.Reporting.Drawing.Unit.Inch(0.27291679382324219D));
            // 
            // reportFooter
            // 
            this.reportFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.44150900840759277D);
            this.reportFooter.Name = "reportFooter";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.44166669249534607D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.quantityDataTextBox,
            this.totalCostDataTextBox,
            this.bookNameDataTextBox});
            this.detail.Name = "detail";
            // 
            // quantityDataTextBox
            // 
            this.quantityDataTextBox.CanGrow = true;
            this.quantityDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.quantityDataTextBox.Name = "quantityDataTextBox";
            this.quantityDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99054968357086182D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.quantityDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.quantityDataTextBox.StyleName = "Data";
            this.quantityDataTextBox.Value = "= Fields.Quantity";
            // 
            // totalCostDataTextBox
            // 
            this.totalCostDataTextBox.CanGrow = true;
            this.totalCostDataTextBox.Format = "{0:N2}";
            this.totalCostDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.490628719329834D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.totalCostDataTextBox.Name = "totalCostDataTextBox";
            this.totalCostDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7770881652832031D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.totalCostDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.totalCostDataTextBox.StyleName = "Data";
            this.totalCostDataTextBox.Value = "= Fields.TotalCost";
            // 
            // bookNameDataTextBox
            // 
            this.bookNameDataTextBox.CanGrow = true;
            this.bookNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020833292976021767D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
            this.bookNameDataTextBox.Name = "bookNameDataTextBox";
            this.bookNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4790878295898438D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.bookNameDataTextBox.StyleName = "Data";
            this.bookNameDataTextBox.Value = "= Fields.BookName";
            // 
            // BillnonMember
            // 
            this.DataSource = this.sqlDataSource1;
            group1.GroupFooter = this.labelsGroupFooterSection;
            group1.GroupHeader = this.labelsGroupHeaderSection;
            group1.Name = "labelsGroup";
            group2.GroupFooter = this.saleIDGroupFooterSection;
            group2.GroupHeader = this.saleIDGroupHeaderSection;
            group2.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.SaleID"));
            group2.Name = "saleIDGroup";
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1,
            group2});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeaderSection,
            this.labelsGroupFooterSection,
            this.saleIDGroupHeaderSection,
            this.saleIDGroupFooterSection,
            this.pageHeader,
            this.reportHeader,
            this.reportFooter,
            this.detail});
            this.Name = "Bill";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Bold = true;
            styleRule2.Style.Font.Italic = false;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule2.Style.Font.Strikeout = false;
            styleRule2.Style.Font.Underline = false;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule3.Style.Color = System.Drawing.Color.Black;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule5.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule5.Style.Font.Name = "Tahoma";
            styleRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4,
            styleRule5});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6.2989668846130371D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.SqlDataSource sqlDataSource1;
        private Telerik.Reporting.GroupHeaderSection labelsGroupHeaderSection;
        private Telerik.Reporting.TextBox saleIDCaptionTextBox;
        private Telerik.Reporting.TextBox bookNameCaptionTextBox;
        private Telerik.Reporting.TextBox quantityCaptionTextBox;
        private Telerik.Reporting.TextBox totalCostCaptionTextBox;
        private Telerik.Reporting.GroupFooterSection labelsGroupFooterSection;
        private Telerik.Reporting.GroupHeaderSection saleIDGroupHeaderSection;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.GroupFooterSection saleIDGroupFooterSection;
        private Telerik.Reporting.PageHeaderSection pageHeader;
        private Telerik.Reporting.TextBox reportNameTextBox;
        private Telerik.Reporting.ReportHeaderSection reportHeader;
        private Telerik.Reporting.TextBox dateCaptionTextBox;
        private Telerik.Reporting.TextBox dateDataTextBox;
        private Telerik.Reporting.TextBox totalDataTextBox;
        private Telerik.Reporting.TextBox getMoneyDataTextBox;
        private Telerik.Reporting.TextBox returnMoneyDataTextBox;
        private Telerik.Reporting.ReportFooterSection reportFooter;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox bookNameDataTextBox;
        private Telerik.Reporting.TextBox quantityDataTextBox;
        private Telerik.Reporting.TextBox totalCostDataTextBox;
        private Telerik.Reporting.TextBox expr1CaptionTextBox;
        private Telerik.Reporting.TextBox expr1DataTextBox;
        private Telerik.Reporting.TextBox expr2DataTextBox;
        private Telerik.Reporting.TextBox expr3DataTextBox;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox returnMoneyCaptionTextBox;
        private Telerik.Reporting.TextBox getMoneyCaptionTextBox;
        private Telerik.Reporting.TextBox totalCaptionTextBox;
    }
}