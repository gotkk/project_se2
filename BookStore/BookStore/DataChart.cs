﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace BookStore
{
    public partial class DataChart : Form
    {
        public DataChart()
        {
            InitializeComponent();
        }

        private void DataChart_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
            loadData();
        }

        private void loadData()
        {
            DataTable dataBook = ConnectionDB.executeSQL("SELECT ISBN,BookName,SUM(Quantity) AS Quantity FROM SaleDetail GROUP BY ISBN, BookName ORDER BY Quantity DESC");
            dgvBook.DataSource = dataBook;

            chartStore.Series[0].XValueMember = dgvBook.Columns[1].DataPropertyName;
            chartStore.Series[0].YValueMembers = dgvBook.Columns[2].DataPropertyName;
            chartStore.DataSource = dgvBook.DataSource;




        }

    }
}
