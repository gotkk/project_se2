﻿namespace BookStore
{
    partial class PrintBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintBill));
            this.reportBill = new Telerik.ReportViewer.WinForms.ReportViewer();
            this.reportBook1 = new Telerik.Reporting.ReportBook();
            this.billMember1 = new BookStore.BillMember();
            this.billnonMember1 = new BookStore.BillnonMember();
            ((System.ComponentModel.ISupportInitialize)(this.billMember1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billnonMember1)).BeginInit();
            this.SuspendLayout();
            // 
            // reportBill
            // 
            this.reportBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportBill.Location = new System.Drawing.Point(0, 0);
            this.reportBill.Name = "reportBill";
            this.reportBill.Size = new System.Drawing.Size(944, 501);
            this.reportBill.TabIndex = 0;
            this.reportBill.ViewMode = Telerik.ReportViewer.WinForms.ViewMode.PrintPreview;
            // 
            // reportBook1
            // 
            this.reportBook1.Reports.Add(this.billMember1);
            this.reportBook1.Reports.Add(this.billnonMember1);
            // 
            // billMember1
            // 
            this.billMember1.Name = "Bill";
            // 
            // billnonMember1
            // 
            this.billnonMember1.Name = "Bill";
            // 
            // PrintBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.reportBill);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(768, 432);
            this.Name = "PrintBill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintBill";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PrintBill_Load);
            ((System.ComponentModel.ISupportInitialize)(this.billMember1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billnonMember1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.ReportViewer.WinForms.ReportViewer reportBill;
        private BillMember billMember1;
        private BillnonMember billnonMember1;
        private Telerik.Reporting.ReportBook reportBook1;
    }
}