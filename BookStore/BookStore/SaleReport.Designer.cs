﻿namespace BookStore
{
    partial class SaleReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaleReport));
            this.reportSalesData = new Telerik.ReportViewer.WinForms.ReportViewer();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.textStart = new System.Windows.Forms.Label();
            this.textEnd = new System.Windows.Forms.Label();
            this.btnLoadData = new System.Windows.Forms.Button();
            this.reportBook1 = new Telerik.Reporting.ReportBook();
            this.reportData1 = new BookStore.ReportData();
            ((System.ComponentModel.ISupportInitialize)(this.reportData1)).BeginInit();
            this.SuspendLayout();
            // 
            // reportSalesData
            // 
            this.reportSalesData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportSalesData.Location = new System.Drawing.Point(12, 73);
            this.reportSalesData.Name = "reportSalesData";
            this.reportSalesData.Size = new System.Drawing.Size(920, 416);
            this.reportSalesData.TabIndex = 0;
            this.reportSalesData.ViewMode = Telerik.ReportViewer.WinForms.ViewMode.PrintPreview;
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "yyyy-MM-dd";
            this.dtpStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(76, 25);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(296, 26);
            this.dtpStart.TabIndex = 1;
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "yyyy-MM-dd hh:mm:ss tt";
            this.dtpEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(464, 25);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(287, 26);
            this.dtpEnd.TabIndex = 2;
            // 
            // textStart
            // 
            this.textStart.AutoSize = true;
            this.textStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textStart.ForeColor = System.Drawing.Color.White;
            this.textStart.Location = new System.Drawing.Point(12, 28);
            this.textStart.Name = "textStart";
            this.textStart.Size = new System.Drawing.Size(58, 20);
            this.textStart.TabIndex = 3;
            this.textStart.Text = "เริ่มวันที่";
            // 
            // textEnd
            // 
            this.textEnd.AutoSize = true;
            this.textEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textEnd.ForeColor = System.Drawing.Color.White;
            this.textEnd.Location = new System.Drawing.Point(396, 30);
            this.textEnd.Name = "textEnd";
            this.textEnd.Size = new System.Drawing.Size(51, 20);
            this.textEnd.TabIndex = 4;
            this.textEnd.Text = "ถึงวันที่";
            // 
            // btnLoadData
            // 
            this.btnLoadData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnLoadData.Location = new System.Drawing.Point(809, 18);
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(123, 44);
            this.btnLoadData.TabIndex = 5;
            this.btnLoadData.Text = "รายงาน";
            this.btnLoadData.UseVisualStyleBackColor = true;
            this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
            // 
            // reportBook1
            // 
            this.reportBook1.Reports.Add(this.reportData1);
            // 
            // reportData1
            // 
            this.reportData1.Name = "ReportData";
            // 
            // SaleReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.btnLoadData);
            this.Controls.Add(this.textEnd);
            this.Controls.Add(this.textStart);
            this.Controls.Add(this.dtpEnd);
            this.Controls.Add(this.dtpStart);
            this.Controls.Add(this.reportSalesData);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(768, 432);
            this.Name = "SaleReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SaleReport";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SaleReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.reportData1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.ReportViewer.WinForms.ReportViewer reportSalesData;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label textStart;
        private System.Windows.Forms.Label textEnd;
        private System.Windows.Forms.Button btnLoadData;
        private ReportData reportData1;
        private Telerik.Reporting.ReportBook reportBook1;
    }
}