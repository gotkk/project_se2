﻿namespace BookStore
{
    partial class Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customer));
            this.tLPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tLPanelbtn = new System.Windows.Forms.TableLayoutPanel();
            this.pictHome = new System.Windows.Forms.PictureBox();
            this.pictEdit = new System.Windows.Forms.PictureBox();
            this.pictDelete = new System.Windows.Forms.PictureBox();
            this.pictAdd = new System.Windows.Forms.PictureBox();
            this.tLPanelSearch = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.pictMagnifier = new System.Windows.Forms.PictureBox();
            this.pictForm = new System.Windows.Forms.PictureBox();
            this.tLPanelLogo = new System.Windows.Forms.TableLayoutPanel();
            this.pictLogo = new System.Windows.Forms.PictureBox();
            this.pictNameShop = new System.Windows.Forms.PictureBox();
            this.dgvCustomer = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tLPanelMain.SuspendLayout();
            this.tLPanelbtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictAdd)).BeginInit();
            this.tLPanelSearch.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictMagnifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictForm)).BeginInit();
            this.tLPanelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictNameShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // tLPanelMain
            // 
            this.tLPanelMain.BackColor = System.Drawing.Color.LightSkyBlue;
            this.tLPanelMain.ColumnCount = 2;
            this.tLPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tLPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tLPanelMain.Controls.Add(this.tLPanelbtn, 1, 2);
            this.tLPanelMain.Controls.Add(this.tLPanelSearch, 1, 0);
            this.tLPanelMain.Controls.Add(this.tLPanelLogo, 0, 0);
            this.tLPanelMain.Controls.Add(this.dgvCustomer, 0, 1);
            this.tLPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tLPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tLPanelMain.Name = "tLPanelMain";
            this.tLPanelMain.RowCount = 3;
            this.tLPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tLPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tLPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tLPanelMain.Size = new System.Drawing.Size(944, 501);
            this.tLPanelMain.TabIndex = 0;
            // 
            // tLPanelbtn
            // 
            this.tLPanelbtn.ColumnCount = 4;
            this.tLPanelbtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tLPanelbtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tLPanelbtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tLPanelbtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tLPanelbtn.Controls.Add(this.pictHome, 3, 0);
            this.tLPanelbtn.Controls.Add(this.pictEdit, 2, 0);
            this.tLPanelbtn.Controls.Add(this.pictDelete, 1, 0);
            this.tLPanelbtn.Controls.Add(this.pictAdd, 0, 0);
            this.tLPanelbtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tLPanelbtn.Location = new System.Drawing.Point(475, 463);
            this.tLPanelbtn.Name = "tLPanelbtn";
            this.tLPanelbtn.RowCount = 1;
            this.tLPanelbtn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tLPanelbtn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tLPanelbtn.Size = new System.Drawing.Size(466, 35);
            this.tLPanelbtn.TabIndex = 1;
            // 
            // pictHome
            // 
            this.pictHome.BackColor = System.Drawing.Color.DarkGray;
            this.pictHome.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictHome.Image = global::BookStore.Properties.Resources.house;
            this.pictHome.Location = new System.Drawing.Point(351, 3);
            this.pictHome.Name = "pictHome";
            this.pictHome.Size = new System.Drawing.Size(112, 29);
            this.pictHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictHome.TabIndex = 7;
            this.pictHome.TabStop = false;
            this.toolTip1.SetToolTip(this.pictHome, "กลับหน้าหลัก (เมนู)");
            this.pictHome.Click += new System.EventHandler(this.pictHome_Click);
            this.pictHome.MouseLeave += new System.EventHandler(this.mplHome);
            this.pictHome.MouseHover += new System.EventHandler(this.mpHome);
            // 
            // pictEdit
            // 
            this.pictEdit.BackColor = System.Drawing.Color.LightYellow;
            this.pictEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictEdit.Image = global::BookStore.Properties.Resources.writing;
            this.pictEdit.Location = new System.Drawing.Point(235, 3);
            this.pictEdit.Name = "pictEdit";
            this.pictEdit.Size = new System.Drawing.Size(110, 29);
            this.pictEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictEdit.TabIndex = 6;
            this.pictEdit.TabStop = false;
            this.toolTip1.SetToolTip(this.pictEdit, "แก้ไขข้อมูลลูกค้า");
            this.pictEdit.Click += new System.EventHandler(this.pictEdit_Click);
            this.pictEdit.MouseLeave += new System.EventHandler(this.mplEdit);
            this.pictEdit.MouseHover += new System.EventHandler(this.mpEdit);
            // 
            // pictDelete
            // 
            this.pictDelete.BackColor = System.Drawing.Color.LightCoral;
            this.pictDelete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictDelete.Image = global::BookStore.Properties.Resources.unfriend;
            this.pictDelete.Location = new System.Drawing.Point(119, 3);
            this.pictDelete.Name = "pictDelete";
            this.pictDelete.Size = new System.Drawing.Size(110, 29);
            this.pictDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictDelete.TabIndex = 5;
            this.pictDelete.TabStop = false;
            this.toolTip1.SetToolTip(this.pictDelete, "ลบข้อมูลลูกค้า");
            this.pictDelete.Click += new System.EventHandler(this.pictDelete_Click);
            this.pictDelete.MouseLeave += new System.EventHandler(this.mplDelete);
            this.pictDelete.MouseHover += new System.EventHandler(this.mpDelete);
            // 
            // pictAdd
            // 
            this.pictAdd.BackColor = System.Drawing.Color.LightGreen;
            this.pictAdd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictAdd.Image = global::BookStore.Properties.Resources.add_friend;
            this.pictAdd.Location = new System.Drawing.Point(3, 3);
            this.pictAdd.Name = "pictAdd";
            this.pictAdd.Size = new System.Drawing.Size(110, 29);
            this.pictAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictAdd.TabIndex = 4;
            this.pictAdd.TabStop = false;
            this.toolTip1.SetToolTip(this.pictAdd, "เพิ่มข้อมูลลูกค้า");
            this.pictAdd.Click += new System.EventHandler(this.pictAdd_Click);
            this.pictAdd.MouseLeave += new System.EventHandler(this.mplAdd);
            this.pictAdd.MouseHover += new System.EventHandler(this.mpAdd);
            // 
            // tLPanelSearch
            // 
            this.tLPanelSearch.ColumnCount = 1;
            this.tLPanelSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tLPanelSearch.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tLPanelSearch.Controls.Add(this.pictForm, 0, 0);
            this.tLPanelSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tLPanelSearch.Location = new System.Drawing.Point(475, 3);
            this.tLPanelSearch.Name = "tLPanelSearch";
            this.tLPanelSearch.RowCount = 2;
            this.tLPanelSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57F));
            this.tLPanelSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43F));
            this.tLPanelSearch.Size = new System.Drawing.Size(466, 104);
            this.tLPanelSearch.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.tbSearch, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictMagnifier, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(460, 39);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tbSearch
            // 
            this.tbSearch.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbSearch.Location = new System.Drawing.Point(3, 10);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(408, 26);
            this.tbSearch.TabIndex = 1;
            this.tbSearch.TextChanged += new System.EventHandler(this.deleteAllSearch);
            this.tbSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            // 
            // pictMagnifier
            // 
            this.pictMagnifier.BackColor = System.Drawing.Color.DarkGray;
            this.pictMagnifier.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictMagnifier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictMagnifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictMagnifier.Image = global::BookStore.Properties.Resources.magnifier;
            this.pictMagnifier.Location = new System.Drawing.Point(417, 3);
            this.pictMagnifier.Name = "pictMagnifier";
            this.pictMagnifier.Size = new System.Drawing.Size(40, 33);
            this.pictMagnifier.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictMagnifier.TabIndex = 2;
            this.pictMagnifier.TabStop = false;
            this.toolTip1.SetToolTip(this.pictMagnifier, "ค้นหา");
            this.pictMagnifier.Click += new System.EventHandler(this.pictMagnifier_Click);
            this.pictMagnifier.MouseLeave += new System.EventHandler(this.mplSearch);
            this.pictMagnifier.MouseHover += new System.EventHandler(this.mpSearch);
            // 
            // pictForm
            // 
            this.pictForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictForm.Image = global::BookStore.Properties.Resources.ลูกค้า;
            this.pictForm.Location = new System.Drawing.Point(3, 3);
            this.pictForm.Name = "pictForm";
            this.pictForm.Size = new System.Drawing.Size(460, 53);
            this.pictForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictForm.TabIndex = 3;
            this.pictForm.TabStop = false;
            // 
            // tLPanelLogo
            // 
            this.tLPanelLogo.ColumnCount = 2;
            this.tLPanelLogo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tLPanelLogo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tLPanelLogo.Controls.Add(this.pictLogo, 0, 0);
            this.tLPanelLogo.Controls.Add(this.pictNameShop, 1, 0);
            this.tLPanelLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tLPanelLogo.Location = new System.Drawing.Point(3, 3);
            this.tLPanelLogo.Name = "tLPanelLogo";
            this.tLPanelLogo.RowCount = 1;
            this.tLPanelLogo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tLPanelLogo.Size = new System.Drawing.Size(466, 104);
            this.tLPanelLogo.TabIndex = 3;
            // 
            // pictLogo
            // 
            this.pictLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictLogo.Image = global::BookStore.Properties.Resources.logo_สีขาว_png;
            this.pictLogo.Location = new System.Drawing.Point(3, 3);
            this.pictLogo.Name = "pictLogo";
            this.pictLogo.Size = new System.Drawing.Size(133, 98);
            this.pictLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictLogo.TabIndex = 0;
            this.pictLogo.TabStop = false;
            // 
            // pictNameShop
            // 
            this.pictNameShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictNameShop.Image = global::BookStore.Properties.Resources.bookshop;
            this.pictNameShop.Location = new System.Drawing.Point(142, 3);
            this.pictNameShop.Name = "pictNameShop";
            this.pictNameShop.Size = new System.Drawing.Size(321, 98);
            this.pictNameShop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictNameShop.TabIndex = 1;
            this.pictNameShop.TabStop = false;
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.AllowUserToAddRows = false;
            this.dgvCustomer.AllowUserToDeleteRows = false;
            this.dgvCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCustomer.BackgroundColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tLPanelMain.SetColumnSpan(this.dgvCustomer, 2);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCustomer.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCustomer.Location = new System.Drawing.Point(3, 113);
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCustomer.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomer.Size = new System.Drawing.Size(938, 344);
            this.dgvCustomer.TabIndex = 4;
            // 
            // Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.tLPanelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(768, 432);
            this.Name = "Customer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Customer_Load);
            this.tLPanelMain.ResumeLayout(false);
            this.tLPanelbtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictAdd)).EndInit();
            this.tLPanelSearch.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictMagnifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictForm)).EndInit();
            this.tLPanelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictNameShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tLPanelMain;
        private System.Windows.Forms.TableLayoutPanel tLPanelbtn;
        private System.Windows.Forms.TableLayoutPanel tLPanelSearch;
        private System.Windows.Forms.TableLayoutPanel tLPanelLogo;
        private System.Windows.Forms.PictureBox pictLogo;
        private System.Windows.Forms.DataGridView dgvCustomer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.PictureBox pictMagnifier;
        private System.Windows.Forms.PictureBox pictAdd;
        private System.Windows.Forms.PictureBox pictDelete;
        private System.Windows.Forms.PictureBox pictEdit;
        private System.Windows.Forms.PictureBox pictHome;
        private System.Windows.Forms.PictureBox pictForm;
        private System.Windows.Forms.PictureBox pictNameShop;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}