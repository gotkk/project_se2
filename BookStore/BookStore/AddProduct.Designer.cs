﻿namespace BookStore
{
    partial class AddProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProduct));
            this.tbISBN = new System.Windows.Forms.TextBox();
            this.tbBookName = new System.Windows.Forms.TextBox();
            this.tbAuthor = new System.Windows.Forms.TextBox();
            this.tbPublisher = new System.Windows.Forms.TextBox();
            this.tbPrintYear = new System.Windows.Forms.TextBox();
            this.tbType = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Nameform = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbQuantity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCost = new System.Windows.Forms.TextBox();
            this.pictCancel = new System.Windows.Forms.PictureBox();
            this.pictOK = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictOK)).BeginInit();
            this.SuspendLayout();
            // 
            // tbISBN
            // 
            this.tbISBN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbISBN.Location = new System.Drawing.Point(174, 62);
            this.tbISBN.MaxLength = 13;
            this.tbISBN.Name = "tbISBN";
            this.tbISBN.Size = new System.Drawing.Size(528, 26);
            this.tbISBN.TabIndex = 6;
            this.tbISBN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            this.tbISBN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber);
            // 
            // tbBookName
            // 
            this.tbBookName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbBookName.Location = new System.Drawing.Point(174, 94);
            this.tbBookName.MaxLength = 80;
            this.tbBookName.Name = "tbBookName";
            this.tbBookName.Size = new System.Drawing.Size(528, 26);
            this.tbBookName.TabIndex = 7;
            this.tbBookName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            // 
            // tbAuthor
            // 
            this.tbAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbAuthor.Location = new System.Drawing.Point(174, 126);
            this.tbAuthor.MaxLength = 70;
            this.tbAuthor.Name = "tbAuthor";
            this.tbAuthor.Size = new System.Drawing.Size(528, 26);
            this.tbAuthor.TabIndex = 8;
            this.tbAuthor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            // 
            // tbPublisher
            // 
            this.tbPublisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbPublisher.Location = new System.Drawing.Point(174, 158);
            this.tbPublisher.MaxLength = 80;
            this.tbPublisher.Name = "tbPublisher";
            this.tbPublisher.Size = new System.Drawing.Size(528, 26);
            this.tbPublisher.TabIndex = 9;
            this.tbPublisher.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            // 
            // tbPrintYear
            // 
            this.tbPrintYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbPrintYear.Location = new System.Drawing.Point(174, 190);
            this.tbPrintYear.MaxLength = 4;
            this.tbPrintYear.Name = "tbPrintYear";
            this.tbPrintYear.Size = new System.Drawing.Size(528, 26);
            this.tbPrintYear.TabIndex = 10;
            this.tbPrintYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            this.tbPrintYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber);
            // 
            // tbType
            // 
            this.tbType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbType.Location = new System.Drawing.Point(174, 222);
            this.tbType.MaxLength = 60;
            this.tbType.Name = "tbType";
            this.tbType.Size = new System.Drawing.Size(528, 26);
            this.tbType.TabIndex = 11;
            this.tbType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(52, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "หมวดหมู่";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(52, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "ปีที่พิมพ์";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(52, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "สำนักพิมพ์";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(52, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "ชื่อผู้แต่ง";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(52, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "ชื่อหนังสือ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(52, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "ISBN";
            // 
            // Nameform
            // 
            this.Nameform.AutoSize = true;
            this.Nameform.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Nameform.Location = new System.Drawing.Point(270, 9);
            this.Nameform.Name = "Nameform";
            this.Nameform.Size = new System.Drawing.Size(221, 37);
            this.Nameform.TabIndex = 18;
            this.Nameform.Text = "เพิ่มข้อมูลหนังสือ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(52, 257);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 20);
            this.label7.TabIndex = 24;
            this.label7.Text = "จำนวน";
            // 
            // tbQuantity
            // 
            this.tbQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbQuantity.Location = new System.Drawing.Point(174, 254);
            this.tbQuantity.MaxLength = 7;
            this.tbQuantity.Name = "tbQuantity";
            this.tbQuantity.Size = new System.Drawing.Size(528, 26);
            this.tbQuantity.TabIndex = 23;
            this.tbQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            this.tbQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(52, 289);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "ราคา";
            // 
            // tbCost
            // 
            this.tbCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tbCost.Location = new System.Drawing.Point(174, 286);
            this.tbCost.MaxLength = 10;
            this.tbCost.Name = "tbCost";
            this.tbCost.Size = new System.Drawing.Size(528, 26);
            this.tbCost.TabIndex = 25;
            this.tbCost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pressEnter);
            this.tbCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pressNumber2);
            // 
            // pictCancel
            // 
            this.pictCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictCancel.Image = ((System.Drawing.Image)(resources.GetObject("pictCancel.Image")));
            this.pictCancel.Location = new System.Drawing.Point(651, 334);
            this.pictCancel.Name = "pictCancel";
            this.pictCancel.Size = new System.Drawing.Size(50, 50);
            this.pictCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictCancel.TabIndex = 22;
            this.pictCancel.TabStop = false;
            this.pictCancel.Click += new System.EventHandler(this.pictCancel_Click);
            this.pictCancel.MouseLeave += new System.EventHandler(this.mplCancel);
            this.pictCancel.MouseHover += new System.EventHandler(this.mpCancel);
            // 
            // pictOK
            // 
            this.pictOK.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pictOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictOK.Image = ((System.Drawing.Image)(resources.GetObject("pictOK.Image")));
            this.pictOK.Location = new System.Drawing.Point(561, 334);
            this.pictOK.Name = "pictOK";
            this.pictOK.Size = new System.Drawing.Size(50, 50);
            this.pictOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictOK.TabIndex = 21;
            this.pictOK.TabStop = false;
            this.pictOK.Click += new System.EventHandler(this.pictOK_Click);
            this.pictOK.MouseLeave += new System.EventHandler(this.mplOK);
            this.pictOK.MouseHover += new System.EventHandler(this.mpOK);
            // 
            // AddProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbCost);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbQuantity);
            this.Controls.Add(this.pictCancel);
            this.Controls.Add(this.pictOK);
            this.Controls.Add(this.Nameform);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbType);
            this.Controls.Add(this.tbPrintYear);
            this.Controls.Add(this.tbPublisher);
            this.Controls.Add(this.tbAuthor);
            this.Controls.Add(this.tbBookName);
            this.Controls.Add(this.tbISBN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 450);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 450);
            this.Name = "AddProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddProduct";
            this.Load += new System.EventHandler(this.AddUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictOK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbAuthor;
        private System.Windows.Forms.TextBox tbPublisher;
        private System.Windows.Forms.TextBox tbPrintYear;
        private System.Windows.Forms.TextBox tbType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Nameform;
        public System.Windows.Forms.TextBox tbISBN;
        public System.Windows.Forms.TextBox tbBookName;
        private System.Windows.Forms.PictureBox pictOK;
        private System.Windows.Forms.PictureBox pictCancel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbQuantity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCost;
    }
}