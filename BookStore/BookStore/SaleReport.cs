﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BookStore
{
    public partial class SaleReport : Form
    {
        public SaleReport()
        {
            InitializeComponent();
        }

        private void SaleReport_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            ConnectionDB.connectsql();
            this.Cursor = Cursors.Default;
        }

        private void LoadDataSales()
        {
            string st = dtpStart.Value.ToString("yyyy-MM-dd");
            string en = dtpEnd.Value.ToString("yyyy-MM-dd hh:mm:ss tt");
            
            DateTime dateStart = Convert.ToDateTime(st);
            DateTime dateEnd = Convert.ToDateTime(en);

           //    dateStart = Convert.ToDateTime(dateStart.ToString("yyyy-MM-dd hh:mm:ss tt"));

            Telerik.Reporting.SqlDataSource report = new Telerik.Reporting.SqlDataSource();
            report.ConnectionString = ConnectionDB.connectionString;
            //         report.SelectCommand = "SELECT Sale.SaleID, Sale.CusID, Sale.ID, Sale.Date, Sale.Total, SaleDetail.ISBN, SaleDetail.BookName, SaleDetail.Quantity, SaleDetail.Cost, SaleDetail.TotalCost, Customer.Title AS CTitle, Customer.FirstName AS CFirstName, Customer.LastName AS CLastName, UserShop.Title AS UTitle, UserShop.FirstName AS UFirstName, UserShop.LastName AS ULastName, Sale.GetMoney, Sale.ReturnMoney FROM Sale INNER JOIN SaleDetail ON Sale.SaleID = SaleDetail.SaleID INNER JOIN Customer ON Sale.CusID = Customer.CusID INNER JOIN UserShop ON Sale.ID = UserShop.ID WHERE Sale.Date BETWEEN '" + dateStart.ToString("yyyy-MM-dd",new CultureInfo("en-US")) + "' AND '"+dateEnd.ToString("yyyy-MM-dd" + "23:59:59",new CultureInfo("en-US"))+"' ";

            report.SelectCommand = "SELECT Sale.SaleID, Sale.CusID, Sale.ID, Sale.Date, Sale.Total, SaleDetail.ISBN, SaleDetail.BookName, SaleDetail.Quantity, SaleDetail.Cost, SaleDetail.TotalCost, Customer.Title AS CTitle, Customer.FirstName AS CFirstName, Customer.LastName AS CLastName, UserShop.Title AS UTitle, UserShop.FirstName AS UFirstName, UserShop.LastName AS ULastName, Sale.GetMoney, Sale.ReturnMoney FROM Sale LEFT OUTER JOIN SaleDetail ON Sale.SaleID = SaleDetail.SaleID LEFT OUTER JOIN Customer ON Sale.CusID = Customer.CusID LEFT OUTER JOIN UserShop ON Sale.ID = UserShop.ID WHERE Sale.Date BETWEEN '" + dateStart.ToString("yyyy-MM-dd", new CultureInfo("en-US")) + "' AND '" + dateEnd.ToString("yyyy-MM-dd hh:mm:ss tt", new CultureInfo("en-US")) + "' ORDER BY Sale.SaleID ";

            //      report.SelectCommand = "SELECT Sale.SaleID, Sale.CusID, Sale.ID, Sale.Date, Sale.Total, SaleDetail.ISBN, SaleDetail.BookName, SaleDetail.Quantity, SaleDetail.Cost, SaleDetail.TotalCost, Customer.Title AS CTitle, Customer.FirstName AS CFirstName, Customer.LastName AS CLastName, UserShop.Title AS UTitle, UserShop.FirstName AS UFirstName, UserShop.LastName AS ULastName, Sale.GetMoney, Sale.ReturnMoney FROM Sale INNER JOIN SaleDetail ON Sale.SaleID = SaleDetail.SaleID INNER JOIN Customer ON Sale.CusID = Customer.CusID INNER JOIN UserShop ON Sale.ID = UserShop.ID";
            reportData1.DataSource = report;
            reportSalesData.ReportSource = reportData1;
            reportSalesData.RefreshReport();
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            LoadDataSales();
        }

       
    }
}
