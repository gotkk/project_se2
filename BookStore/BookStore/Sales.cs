﻿using BookStore.Process;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class Sales : Form
    {
        public Sales()
        {
            InitializeComponent();
        }

        private void Sales_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            ConnectionDB.connectsql();
            setClear();
            this.Cursor = Cursors.Default;
        }

        private void pictHome_Click(object sender, EventArgs e)
        {
            if (dgvSales.RowCount != 0)
            {
                MessageBox.Show("คุณทำรายการยังไม่เสร็จสิ้น ไม่สามารถกลับหน้าหลัก (เมนู) ได้ กรุณาจัดการรายการสินค้าให้เรียบร้อย", "แจ้งเตือน",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            this.Close();
        }

        // button color
        private static Color mp = Color.SteelBlue;
        private static Color mpl = Color.LightSkyBlue;
        private void mpHome(object sender, EventArgs e)
        {
            pictHome.BackColor = mp;
        }

        private void mplHome(object sender, EventArgs e)
        {
            pictHome.BackColor = Color.DarkGray;
        }

        private void mpAdd(object sender, EventArgs e)
        {
            pictAdd.BackColor = mp;
        }

        private void mplAdd(object sender, EventArgs e)
        {
            pictAdd.BackColor = Color.LightGreen;
        }

        private void mpDel(object sender, EventArgs e)
        {
            pictDelete.BackColor = mp;
        }

        private void mplDel(object sender, EventArgs e)
        {
            pictDelete.BackColor = Color.LightCoral;
        }

        private void mpCheckBill(object sender, EventArgs e)
        {
            pictCheckBill.BackColor = mp;
        }

        private void mplCheckBill(object sender, EventArgs e)
        {
            pictCheckBill.BackColor = Color.LightYellow;
        }

        private void mpReport(object sender, EventArgs e)
        {
            pictReport.BackColor = mp;
        }

        private void mplReport(object sender, EventArgs e)
        {
            pictReport.BackColor = Color.DarkGray;
        }


        private void docID()
        {
            int id = 0;// error load
            DataTable loadDocID = ConnectionDB.executeSQL("SELECT COUNT(SaleID) as countID FROM Sale");
            id = Convert.ToInt32(loadDocID.Rows[0]["countID"].ToString())+1;
                  int countNum = Convert.ToInt32(id.ToString().Length);
                  switch (countNum)
                  {
                      case 1:
                          {
                              tbDocID.Text = "000000000000" + id;
                              break;
                          }
                      case 2:
                          {
                              tbDocID.Text = "00000000000" + id;
                              break;
                          }
                      case 3:
                          {
                              tbDocID.Text = "0000000000" + id;
                              break;
                          }
                      case 4:
                          {
                              tbDocID.Text = "000000000" + id;
                              break;
                          }
                      case 5:
                          break;
                      case 6:
                          {
                              tbDocID.Text = "00000000" + id;
                              break;
                          }
                      case 7:
                          {
                              tbDocID.Text = "0000000" + id;
                              break;
                          }
                      case 8:
                          {
                              tbDocID.Text = "000000" + id;
                              break;
                          }
                      case 9:
                          {
                              tbDocID.Text = "0000" + id;
                              break;
                          }
                      case 10:
                          {
                              tbDocID.Text = "000" + id;
                              break;
                          }
                      case 11:
                          {
                              tbDocID.Text = "00" + id;
                              break;
                          }
                      case 12:
                          {
                              tbDocID.Text = "0" + id;
                              break;
                          }
                      default:
                          {
                              // id 13 char
                              tbDocID.Text = id.ToString();
                              break;
                          }

                  }
        }

        private void txchinputProductID(object sender, EventArgs e)
        {
            if (tbProductID.Text.Length == 13)
            {
                DataTable productIDInput = ConnectionDB.executeSQL("SELECT * FROM Store WHERE ISBN = '" + tbProductID.Text + "'");
                if (productIDInput.Rows.Count > 0)
                {
                    // highlight
                    nupQty.Focus();
                    nupQty.Value = 1;
                    nupQty.Select(0, nupQty.Text.Length);
                }
                else
                {
                    MessageBox.Show("กรุณากรอกรหัสหนังสือให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbProductID.Clear();
                    tbProductID.Focus();
                    nupQty.Value = 0;
                }
            }
           
        }

        private void addProductToList()
        {
            int newQTY = Convert.ToInt32(nupQty.Value);
            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                if (dgvSales.Rows[i].Cells[0].Value.ToString().Equals(tbProductID.Text))
                {
                    newQTY = Convert.ToInt32(dgvSales.Rows[i].Cells[2].Value.ToString()) + newQTY;
                    dgvSales.Rows.RemoveAt(dgvSales.Rows[i].Cells[2].RowIndex);
                    showTotal();
                    break;
                }

            }

            DataTable product = ConnectionDB.executeSQL("SELECT Name, Cost FROM Store WHERE ISBN = '" + tbProductID.Text + "'");
            if (product.Rows.Count == 0)
            {
                MessageBox.Show("กรุณากรอกรหัสหนังสือให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbProductID.Clear();
                tbProductID.Focus();
                return;
            }

            // show in DGV
            string bookname = product.Rows[0][0].ToString();
            double cost = Convert.ToDouble(product.Rows[0][1]);
            double total = newQTY * cost;
            dgvSales.Rows.Add(tbProductID.Text, bookname, newQTY, cost, total);

            // last row focus when insert
            int dgvcount = dgvSales.Rows.Count;
            dgvSales.CurrentCell = dgvSales.Rows[dgvcount - 1].Cells[0];

            showTotal();
            checkDiscountForCustomer();

            // go to next product
            tbProductID.Clear();
            nupQty.Value = 0;
            tbProductID.Focus();
        }

        private void showTotal()
        {
            // show total (sum)
            double sum = 0;
            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                sum += Convert.ToInt32(dgvSales.Rows[i].Cells[4].Value);
            }
            tbTotal.Text = sum.ToString("0.00");

        }

        private void kdownEnterAddProduct(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                addProductToList();
            }
        }



        private void pressNumber(object sender, KeyPressEventArgs e)
        {
            PCValueInput pc = new PCValueInput();
            pc.pressNumber(e);
        }

        

        private void txchinputMoney(object sender, EventArgs e)
        {
            showReturnMenory();
        }

        private void showReturnMenory()
        {
            if (tbGetMoney.Text == "")
            {
                // break when delete getMoney
                tbReturnMoney.Text = (0).ToString("0.00");
                return;
            }
            double returnMoney = Convert.ToDouble(tbGetMoney.Text) - Convert.ToDouble(tbTotal.Text);
            tbReturnMoney.Text = returnMoney.ToString("0.00");
        }



        private void txchinputCusID(object sender, EventArgs e)
        {
            if (tbCusID.Text.Length == 13)
            {
                if (tbCusID.Text == "0000000000000")
                {
                    MessageBox.Show("กรุณากรอกรหัสลูกค้าให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbCusID.Clear();
                    tbCusID.Focus();
                    textDiscount.Text = "";
                    return;
                }

                DataTable cusIDInput = ConnectionDB.executeSQL("SELECT * FROM Customer WHERE CusID = '" + tbCusID.Text + "'");
                if (cusIDInput.Rows.Count > 0)
                {
                  //  chDiscount.Checked = true;
                    textDiscount.Text = "ลูกค้าได้รับส่วนลด 5%";
                }
                else
                {
                    MessageBox.Show("กรุณากรอกรหัสลูกค้าให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbCusID.Clear();
                    tbCusID.Focus();
                //    chDiscount.Checked = false;
                    textDiscount.Text = "";
                }
            }
            else
            {
            //    chDiscount.Checked = false;
                textDiscount.Text = "";
            }
        }

        private void checkDiscountForCustomer()
        {
          //  if(chDiscount.Checked == true)
            if(textDiscount.Text.Equals("ลูกค้าได้รับส่วนลด 5%"))
            {
                double cost = (Convert.ToDouble(tbTotal.Text) * 95) / 100;
                tbTotal.Text = cost.ToString("0.00");  
            }
            if (textDiscount.Text.Equals(""))
            {
                showTotal();
            }

        }

        private void txchTotalM(object sender, EventArgs e)
        {
            showReturnMenory();
        }

        private void pictCheckBill_Click(object sender, EventArgs e)
        {
            checkCusIDTFtoSave();
        }

        private void checkCusIDTFtoSave()
        {
            this.Cursor = Cursors.AppStarting;
            DataTable cusIDInput = ConnectionDB.executeSQL("SELECT * FROM Customer WHERE CusID = '" + tbCusID.Text + "'");
            if (cusIDInput.Rows.Count > 0||tbCusID.Text=="")
            {
                saveDataSales();
            }
            else
            {
                //then cusid not full that false
                MessageBox.Show("รหัสลูกค้าไม่ถูกต้อง   ","แจ้งเตือน",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                tbCusID.Clear();
            }
            this.Cursor = Cursors.Default;
        }

        private void kdownEnterSave(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                checkCusIDTFtoSave();
            }
        }

        private void saveDataSales()
        {
            // check input money TF
            if (Convert.ToDouble(tbReturnMoney.Text) < 0 || (tbGetMoney.Text.Length == 0 && Convert.ToDouble(tbReturnMoney.Text) == 0))
            {
                MessageBox.Show("กรุณากรอกจำนวนเงินที่ได้รับให้ถูกค้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbGetMoney.Clear();
                tbGetMoney.Focus();
                return;
            }

            // check product
            if (dgvSales.Rows.Count == 0)
            {
                MessageBox.Show("กรุณาทำรายการซื้อก่อนดำเนินการบันทึกข้อมูล", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbProductID.Focus();
                return;
            }


            // check store quantity
            int storeQTY, dgvQTY;
            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                DataTable quantityStore = ConnectionDB.executeSQL("SELECT Quantity FROM Store WHERE ISBN = '" + dgvSales.Rows[i].Cells[0].Value.ToString() + "'");
                storeQTY = Convert.ToInt32(quantityStore.Rows[0][0]);
                dgvQTY = Convert.ToInt32(dgvSales.Rows[i].Cells[2].Value);
                int totalQTY = storeQTY - dgvQTY;
                if (totalQTY < 0)
                {
                    MessageBox.Show("สินค้าในคลังมีไม่เพียงพอ กรุณากรอกจำนวนสินค้าให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }                
            }



            if (MessageBox.Show("คุณต้องการบันทึกข้อมูลการขายหนังสือหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                if(tbCusID.Text == "")  
                {
                    //set CusID = 0000000000000 then nonMember
                    confirmedSaveDataSales("0000000000000");
                }
                else
                {
                    confirmedSaveDataSales(tbCusID.Text);
                }
            }
            

        }

        private void confirmedSaveDataSales(string strCusID)
        {
            ConnectionDB.executeSQL("INSERT INTO Sale (SaleID,Date,Total,GetMoney,ReturnMoney,CusID,ID) VALUES('" + tbDocID.Text + "',SYSDATETIME(),'" + tbTotal.Text + "','" + tbGetMoney.Text + "','" + tbReturnMoney.Text + "','"+ strCusID + "', '" + textUserName.Text + "')"); //save main

            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                //save saledetail   
                ConnectionDB.executeSQL("INSERT INTO SaleDetail (SaleID ,ISBN ,BookName ,Quantity ,Cost ,TotalCost) VALUES ('" + tbDocID.Text + "','" + dgvSales.Rows[i].Cells[0].Value.ToString() + "','" + dgvSales.Rows[i].Cells[1].Value.ToString() + "','" + dgvSales.Rows[i].Cells[2].Value.ToString() + "','" + dgvSales.Rows[i].Cells[3].Value.ToString() + "','" + dgvSales.Rows[i].Cells[4].Value.ToString() + "')");

                //cutstock                                                                                                                                                                                                                                                                                                                                                       
                ConnectionDB.executeSQL("UPDATE Store SET Quantity = Quantity-'" + dgvSales.Rows[i].Cells[2].Value.ToString() + "'WHERE ISBN = '" + dgvSales.Rows[i].Cells[0].Value.ToString() + "'");

                // check to line notify
                checkQTYNotify(i, dgvSales.Rows[i].Cells[0].Value.ToString());
            }

            PrintBill bill = new PrintBill();
            bill.textDocID = tbDocID.Text;
            // controlBill
            if (tbCusID.Text != "")
            {
                bill.member = true;
            }
            else
            {
                bill.member = false;
            }
            bill.ShowDialog();

            MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
            setClear();
        }

        private void checkQTYNotify(int i, string ISBN)
        {
            DataTable qty = ConnectionDB.executeSQL("SELECT Quantity FROM Store WHERE ISBN = '"+ISBN+"'");
            int quantity = Convert.ToInt32(qty.Rows[0][0].ToString());
            if (quantity <=3 && quantity >= 0)
            {
                string textToLine = "รหัสสินค้า " + dgvSales.Rows[i].Cells[0].Value.ToString() + " " + dgvSales.Rows[i].Cells[1].Value.ToString() + " มียอดคงเหลือ " + qty.Rows[0][0].ToString() + " เล่ม";
                lineNotify(textToLine);
            }
        }

        private void lineNotify(String notify)
        {
            string token = "cgHec6EhxaFRw9QS2gu3VuwaCN6ImFyrBegcX0kppDn";
            try
            {
                var request = (HttpWebRequest)WebRequest.Create("https://notify-api.line.me/api/notify");
                var postData = string.Format("message={0}", notify);
                var data = Encoding.UTF8.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.Headers.Add("Authorization", "Bearer " + token);

                using (var stream = request.GetRequestStream()) stream.Write(data, 0, data.Length);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private void setClear()
        {
            dgvSales.Rows.Clear();     
            tbCusID.Clear();
            tbProductID.Clear();
            tbGetMoney.Clear();
            nupQty.Value = 0;
            double b = 0;
            tbTotal.Text = b.ToString("0.00");
            tbReturnMoney.Text = b.ToString("0.00");
            this.ActiveControl = tbProductID;
            docID();
        }

        private void pictAdd_Click(object sender, EventArgs e)
        {
            addProductToList();
        }

        private void pictDelete_Click(object sender, EventArgs e)
        {
            if (dgvSales.Rows.Count == 0)
            {
                MessageBox.Show("กรุณาเลือกรายการที่จะยกเลิกการซื้อ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            dgvSales.Rows.RemoveAt(dgvSales.CurrentCell.RowIndex);
            showTotal();
        }

        private void pictReport_Click(object sender, EventArgs e)
        {
            this.Hide();
            SaleReport reportSale = new SaleReport();
            reportSale.ShowDialog();
            this.Show();
        }

        private void textDisShow(object sender, EventArgs e)
        {
            checkDiscountForCustomer();
        }

        private void frmClosing(object sender, FormClosingEventArgs e)
        {
            // It have products
            if (dgvSales.Rows.Count != 0)
            {
                MessageBox.Show("คุณทำรายการขาย ไม่เสร็จสิ้น ไม่สามารถปิดหน้านี้ได้ กรุณาจัดการรายการสินค้าให้เรียบร้อย ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            
        }

        private void kdownCheckCusIDtoSave(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                checkCusIDTFtoSave();
            }
        }

        private void pressNumber2(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
            }

            //check if '.' , ',' pressed
            char sepratorChar = 's';
            if (e.KeyChar == '.')
            {
                // check if it's in the beginning of text not accept
                if (tbGetMoney.Text.Length == 0)
                {
                    MessageBox.Show("ไม่สามารถกรอกข้อมูลขึ้นต้นด้วย (.) ได้ ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
                // check if it's in the beginning of text not accept
                if (tbGetMoney.SelectionStart == 0)
                {
                    //                    MessageBox.Show("ไม่สามารถกรอกข้อมูลขึ้นต้นด้วย (.)2", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
                // check if there is already exist a '.' , ','
                if (alreadyExist(tbGetMoney.Text, ref sepratorChar))
                {
                    MessageBox.Show("ไม่สามรถกรอกข้อมูล (.) ซ้ำได้ ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Handled = true;
                }
                //check if '.' or ',' is in middle of a number and after it is not a number greater than 99
                if (tbGetMoney.SelectionStart != tbGetMoney.Text.Length && e.Handled == false)
                {
                    // '.' or ',' is in the middle
                    string AfterDotString = tbGetMoney.Text.Substring(tbGetMoney.SelectionStart);

                    if (AfterDotString.Length > 2)
                    {
                        //                        MessageBox.Show("ไม่สามารถกรอกข้อมูลตัวเลขทศนิยมเกิน 2 ตำแหน่ง1 ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Handled = true;
                    }
                }
            }
            //check if a number pressed

            if (Char.IsDigit(e.KeyChar))
            {
                //check if a coma or dot exist
                if (alreadyExist(tbGetMoney.Text, ref sepratorChar))
                {
                    int sepratorPosition = tbGetMoney.Text.IndexOf(sepratorChar);
                    string afterSepratorString = tbGetMoney.Text.Substring(sepratorPosition + 1);
                    if (tbGetMoney.SelectionStart > sepratorPosition && afterSepratorString.Length > 1)
                    {
                        MessageBox.Show("ไม่สามารถกรอกข้อมูลตัวเลขทศนิยมเกิน 2 ตำแหน่ง ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Handled = true;
                    }

                }
            }
        }

        private bool alreadyExist(string _text, ref char KeyChar)
        {
            if (_text.IndexOf('.') > -1)
            {
                KeyChar = '.';
                return true;
            }
            if (_text.IndexOf(',') > -1)
            {
                KeyChar = ',';
                return true;
            }
            return false;
        }
    }
}
