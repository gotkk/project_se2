﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class Customer : Form
    {
        public Customer()
        {
            InitializeComponent();
        }

        private void Customer_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            ConnectionDB.connectsql();
            LoadData();
          //  SetColumnDGV();
            this.Cursor = Cursors.Default;
        }
        
        private void LoadData()
        {
            // DataGridviewLoadData
            DataTable loaddata = ConnectionDB.executeSQL("SELECT * FROM Customer");
            dgvCustomer.DataSource = loaddata;
            dgvCustomer.Columns[0].HeaderText = "รหัสลูกค้า";
            dgvCustomer.Columns[1].HeaderText = "คำนำหน้า";
            dgvCustomer.Columns[2].HeaderText = "ชื่อ";
            dgvCustomer.Columns[3].HeaderText = "นามสกุล";
            dgvCustomer.Columns[4].HeaderText = "ที่อยู่";
            dgvCustomer.Columns[5].HeaderText = "เบอร์โทร";
        }

        private void SetColumnDGV()
        {
            DataGridViewColumn column0 = dgvCustomer.Columns[0];
            column0.Width = 120;
            DataGridViewColumn column1 = dgvCustomer.Columns[1];
            column1.Width = 90;
            DataGridViewColumn column2 = dgvCustomer.Columns[2];
            column2.Width = 160;
            DataGridViewColumn column3 = dgvCustomer.Columns[3];
            column3.Width = 160;
            DataGridViewColumn column5 = dgvCustomer.Columns[5];
            column5.Width = 140;
        }

        private void SearchCustomer()
        {
            //Search
            DataTable searchData = ConnectionDB.executeSQL("SELECT * FROM Customer WHERE CusID LIKE'%" + tbSearch.Text + "%' OR Title LIKE'%" + tbSearch.Text + "%' OR FirstName LIKE'%" + tbSearch.Text + "%' OR LastName LIKE'%" + tbSearch.Text + "%' OR Address LIKE'%" + tbSearch.Text + "%' OR Tel LIKE'%" + tbSearch.Text + "%'");
            if (tbSearch.Text == "")
            {
                LoadData();
            }
            else
            {
                dgvCustomer.DataSource = searchData;
            }
        }

        private void pictMagnifier_Click(object sender, EventArgs e)
        {
            SearchCustomer();
        }

        private void pictAdd_Click(object sender, EventArgs e)
        {
            AddCustomer addDataa = new AddCustomer();
            addDataa.ShowDialog();
            LoadData();
        }

        private void pictDelete_Click(object sender, EventArgs e)
        {
            // warning when you delete that chart will cann't show this data
            DialogResult result = MessageBox.Show("ในรายงานแสดงรายการขาย จะไม่สามารถแสดงข้อมูลในส่วนที่ถูกลบ ไปแล้ว ได้  <Yes> เพื่อดำเนอนการต่อ  ", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,MessageBoxDefaultButton.Button2);
            if (result == DialogResult.No)
            {
                return;
            }


            // Deny delete  data Cus None.
            if (dgvCustomer.SelectedCells[0].Value.ToString() == "0000000000000")
            {
                MessageBox.Show("ไม่สามารถลบข้อมูลของลูกค้า รหัส 0000000000000 ได้", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Delete Only 1 row Select
            if (dgvCustomer.SelectedRows.Count > 1)
            {
                MessageBox.Show("กรุณาเลือกแค่ 1 แถวที่จะลบข้อมูล", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DialogResult dialogResult = MessageBox.Show("คุณแน่ใจที่จะลบข้อมูลของลูกค้าที่เลือกหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (dialogResult == DialogResult.Yes)
            {
                ConnectionDB.executeSQL("DELETE FROM Customer WHERE CusID = '" + dgvCustomer.SelectedCells[0].Value.ToString() + "'");
                LoadData();
            }
        }

        private void pictEdit_Click(object sender, EventArgs e)
        {
            if (dgvCustomer.SelectedCells[0].Value.ToString() == "0000000000000")
            {
                MessageBox.Show("ไม่สามารถแก้ไขข้อมูลของลูกค้า รหัส 0000000000000 ได้", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            EditCustomer editCustomer = new EditCustomer();
            editCustomer.tbCusID.Text = this.dgvCustomer.SelectedCells[0].Value.ToString();
            editCustomer.tbTitle.Text = this.dgvCustomer.SelectedCells[1].Value.ToString();
            editCustomer.tbFirstName.Text = this.dgvCustomer.SelectedCells[2].Value.ToString();
            editCustomer.tbLastName.Text = this.dgvCustomer.SelectedCells[3].Value.ToString();
            editCustomer.tbAddress.Text = this.dgvCustomer.SelectedCells[4].Value.ToString();
            editCustomer.tbTel.Text = this.dgvCustomer.SelectedCells[5].Value.ToString();
            editCustomer.ShowDialog();
            LoadData();
        }

        private void pictHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // button color
        private static Color mp = Color.SteelBlue;
      //  private static Color mpl = Color.LightSkyBlue;
        private void mpAdd(object sender, EventArgs e)
        {
            pictAdd.BackColor = mp;
        }

        private void mplAdd(object sender, EventArgs e)
        {
            pictAdd.BackColor = Color.LightGreen;
        }

        private void mpDelete(object sender, EventArgs e)
        {
            pictDelete.BackColor = mp;
        }

        private void mplDelete(object sender, EventArgs e)
        {
            pictDelete.BackColor = Color.LightCoral;
        }

        private void mpEdit(object sender, EventArgs e)
        {
            pictEdit.BackColor = mp;
        }

        private void mplEdit(object sender, EventArgs e)
        {
            pictEdit.BackColor = Color.LightYellow;
        }

        private void mpHome(object sender, EventArgs e)
        {
            pictHome.BackColor = mp;
        }

        private void mplHome(object sender, EventArgs e)
        {
            pictHome.BackColor = Color.DarkGray;
        }

        private void mpSearch(object sender, EventArgs e)
        {
            pictMagnifier.BackColor = mp;
        }

        private void mplSearch(object sender, EventArgs e)
        {
            pictMagnifier.BackColor = Color.DarkGray;
        }

        private void pressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchCustomer();
            }
        }

        private void deleteAllSearch(object sender, EventArgs e)
        {
            if(tbSearch.Text == "")
            {
                LoadData();
            }
        }
    }
}
