﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore.Process
{
    class PCValueInput
    {
        public void pressNumber(KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13))
            {
                e.Handled = true;
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void dontNumber(KeyPressEventArgs e)
        {
            if ((e.KeyChar > 47 && e.KeyChar < 58))
            {
                e.Handled = true;
                MessageBox.Show("ไม่สามารถกรอกข้อมูลที่เป็นตัวเลขได้ ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void pressNumber2(object sender, KeyPressEventArgs e)
        {
                //accept decimal (.)
                if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13) && (e.KeyChar != 46))
                {
                    e.Handled = true;
                    MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

        }

        public void pressCharacter(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                MessageBox.Show("กรุณากรอกข้อมูลตัวอักษรให้ถูกต้อง ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                int a = Convert.ToChar(e.KeyChar);
                MessageBox.Show(a.ToString(), "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
            }
        }

        public void pressCharacter2(KeyPressEventArgs e)
        {
            //accept decimal (.)
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && e.KeyChar != 46)
            {
                MessageBox.Show("กรุณากรอกข้อมูลตัวอักษรให้ถูกต้อง ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
            }
        }
    }
}
