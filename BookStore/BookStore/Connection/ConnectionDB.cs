﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

public class ConnectionDB
{
    public static string connectionString;

    public static void connectsql()
    {
        //      Azure Database

        connectionString = "Server=tcp:dbshop.database.windows.net,1433;Initial Catalog=BookShop;Persist Security Info=False;User ID=BookShop;Password=Shop1234;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        /*      Windows Authentacation
                connectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=BookStore;Integrated Security=True";
        */


        /*      SQL Sever Authentacation
                connectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=BookStore; User ID = Qwer; password=1234;";
        */


        /*      SQL IP
                connectionString = "Data Source=10.5.50.35;Initial Catalog=BookStore;User ID=Qwer;Password=1234;";
        */
    }

    public static DataTable executeSQL(string sql)
    {
        SqlDataAdapter dtAdapter = default(SqlDataAdapter);
        SqlConnection objConnection = new SqlConnection();
        DataTable dt = new DataTable();
        try
        {
            objConnection.ConnectionString = connectionString;
            objConnection.Open();
            dtAdapter = new SqlDataAdapter(sql, objConnection);
            dtAdapter.Fill(dt);
            objConnection.Close();
            objConnection = null;
            return dt;
        }
        catch (Exception exx)
        {
            Console.Write(exx.ToString());
            dt = null;
        }
        return dt;
    }

}
