﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class PrintBill : Form
    {
        public PrintBill()
        {
            InitializeComponent();
        }

        private void PrintBill_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
            if (member)
            {
                loadBillMember();
            }
            else
            {
                loadBillnonMenber();
            }
        }

        // set data from form Sales
        public string textDocID = "";
        public bool member = false;

        private void loadBillMember()
        {
            Telerik.Reporting.SqlDataSource report = new Telerik.Reporting.SqlDataSource();
            report.ConnectionString = ConnectionDB.connectionString;
            report.SelectCommand = "SELECT Sale.SaleID, Sale.CusID, Sale.ID, Sale.Date, Sale.Total, Sale.GetMoney, Sale.ReturnMoney, SaleDetail.BookName, SaleDetail.Quantity, SaleDetail.TotalCost, Customer.Title, Customer.FirstName, Customer.LastName, UserShop.Title AS Expr1, UserShop.FirstName AS Expr2, UserShop.LastName AS Expr3 FROM Sale INNER JOIN SaleDetail ON Sale.SaleID = SaleDetail.SaleID INNER JOIN Customer ON Sale.CusID = Customer.CusID INNER JOIN UserShop ON Sale.ID = UserShop.ID WHERE SaleDetail.SaleID = '" + textDocID + "' ";
            billMember1.DataSource = report;
            reportBill.ReportSource = billMember1;
            reportBill.RefreshReport();
        }

        private void loadBillnonMenber()
        {
            // string docSaleID = label1.Text;
            Telerik.Reporting.SqlDataSource report = new Telerik.Reporting.SqlDataSource();
            report.ConnectionString = ConnectionDB.connectionString;
            report.SelectCommand = "SELECT Sale.SaleID, Sale.ID, Sale.Date, Sale.Total, Sale.GetMoney, Sale.ReturnMoney, SaleDetail.BookName, SaleDetail.Quantity, SaleDetail.TotalCost, UserShop.Title, UserShop.FirstName, UserShop.LastName FROM Sale INNER JOIN SaleDetail ON Sale.SaleID = SaleDetail.SaleID INNER JOIN UserShop ON Sale.ID = UserShop.ID WHERE SaleDetail.SaleID = '" + textDocID + "' ";
            billnonMember1.DataSource = report;
            reportBill.ReportSource = billnonMember1;
            reportBill.RefreshReport();
        }
    }
}
