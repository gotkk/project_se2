﻿using BookStore.Process;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class AddUser : Form
    {
        public AddUser()
        {
            InitializeComponent();
        }
        private void AddUser_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
        }

        private void pictOK_Click(object sender, EventArgs e)
        {
            checkDatatoAdd();
        }

        private void pictCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool blockSQLInjection()
        {
            //block Sql Injection
            string x;
            for (int i = 0; i < tbID.Text.Length; i++)
            {
                x = tbID.Text[i].ToString();
                if (x.Equals("\"") || x.Equals("\'"))
                {
                    return true;
                }
            }
            for (int i = 0; i < tbPassword.Text.Length; i++)
            {
                x = tbPassword.Text[i].ToString();
                if (x.Equals("\"") || x.Equals("\'"))
                {
                    return true;
                }
            }
            return false;
        }

        public void checkDatatoAdd()
        {
            if (tbUserID.Text == "" || tbID.Text == "" || tbPassword.Text == "" || tbTitle.Text == "" || tbFirstName.Text == "" || tbLastName.Text == "" || tbAddress.Text == ""||tbTel.Text==""|| tbSalary.Text == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (tbUserID.Text.Length < 13)
            {
                MessageBox.Show("กรุณากรอกรหัสพนักงานให้ครบ 13 หลัก", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbUserID.Focus();
                return;
            }

            //block Sql Injection
            if (blockSQLInjection())
            {
                MessageBox.Show("ไม่สามารถตั้งชื่อผู้ใช้ หรือ รหัสผ่านที่มี single quote ( \' ) หรือ double quote ( \" ) ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbID.Clear();
                tbPassword.Clear();
                tbID.Focus();
                return;
            }


            //Check userID Used to in db
            DataTable checkdataUserID = ConnectionDB.executeSQL("SELECT * FROM UserShop WHERE UserID='" + tbUserID.Text + "'");
            if (checkdataUserID.Rows.Count > 0)
            {
                MessageBox.Show("รหัสพนักงานนี้ถูกใช้ไปแล้ว กรุณากรอกรหัสพนักงานใหม่", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbUserID.Clear();
                tbUserID.Focus();
                return;
            }
            DataTable checkdataID = ConnectionDB.executeSQL("SELECT * FROM UserShop WHERE ID='" + tbID.Text + "'");
            if (checkdataID.Rows.Count > 0)
            {
                MessageBox.Show("ชื่อผู้ใช้พนักงานนี้ถูกใช้ไปแล้ว กรุณากรอกชื่อผู้ใช้พนักงานใหม่", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbID.Clear();
                tbID.Focus();
                return;
            }

            ConnectionDB.executeSQL("INSERT INTO UserShop (UserID,ID,Password,Title,FirstName,LastName,Address,Tel,Salary) VALUES('" + tbUserID.Text + "','"+tbID.Text+"','" + tbPassword.Text + "','" + tbTitle.Text + "','" + tbFirstName.Text + "','" + tbLastName.Text + "','" + tbAddress.Text + "','"+tbTel.Text+"','"+tbSalary.Text+"')");
            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void pressNumber(object sender, KeyPressEventArgs e)
        {
            /*    if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13))
                {
                    e.Handled = true;
                    MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }*/
            PCValueInput pc = new PCValueInput();
            pc.pressNumber(e);
        }

        private void pressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                checkDatatoAdd();
            }
        }

        //button zoom
        private void mpOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(60, 60);
        }

        private void mplOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(50, 50);
        }

        private void mpCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(60, 60);
        }

        private void mplCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(50, 50);
        }

        private void dontNumber(object sender, KeyPressEventArgs e)
        {
            /*     if ((e.KeyChar > 47 && e.KeyChar < 58))
                 {
                     e.Handled = true;
                     MessageBox.Show("ไม่สามารถกรอกข้อมูลที่เป็นตัวเลขได้", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                 }*/
            PCValueInput pc = new PCValueInput();
            pc.dontNumber(e);
        }

        private void dontNumber2(object sender, KeyPressEventArgs e)
        {
            PCValueInput pc = new PCValueInput();
            pc.dontNumber(e);
        }
    }
}
