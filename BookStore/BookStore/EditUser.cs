﻿using BookStore.Process;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class EditUser : Form
    {
        public EditUser()
        {
            InitializeComponent();
        }

        private void EditUser_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
            this.ActiveControl = tbID;
        }

        private void pictOK_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void pictCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool blockSQLInjection()
        {
            //block Sql Injection
            string x;
            for (int i = 0; i < tbID.Text.Length; i++)
            {
                x = tbID.Text[i].ToString();
                if (x.Equals("\"") || x.Equals("\'"))
                {
                    return true;
                }
            }
            for (int i = 0; i < tbPassword.Text.Length; i++)
            {
                x = tbPassword.Text[i].ToString();
                if (x.Equals("\"") || x.Equals("\'"))
                {
                    return true;
                }
            }
            return false;
        }


        private void EditData()
        {
            if (tbPassword.Text == "" ||tbID.Text == "" || tbTitle.Text == "" || tbFirstName.Text == "" || tbLastName.Text == "" || tbAddress.Text == "" || tbTel.Text == ""||tbSalary.Text == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //block Sql Injection
            if (blockSQLInjection())
            {
                MessageBox.Show("ไม่สามารถตั้งชื่อผู้ใช้ หรือ รหัสผ่านที่มี single quote ( \' ) หรือ double quote ( \" ) ", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbID.Clear();
                tbPassword.Clear();
                tbID.Focus();
                return;
            }


            ConnectionDB.executeSQL("UPDATE UserShop SET ID = '"+tbID.Text+"', Password='"+tbPassword.Text+"', Title='" + tbTitle.Text + "',FirstName='" + tbFirstName.Text + "',LastName='" + tbLastName.Text + "',Address='" + tbAddress.Text + "',Tel='" + tbTel.Text + "',Salary = '"+tbSalary.Text+"' WHERE UserID='" + tbUserID.Text + "'");
            MessageBox.Show("แก้ไขข้อมูลพนักงานแล้ว", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void pressNumber(object sender, KeyPressEventArgs e)
        {
            /*       if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13) )
                   {
                       e.Handled = true;
                       MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                   }*/
            PCValueInput pc = new PCValueInput();
            pc.pressNumber(e);
        }

        private void pressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EditData();
            }
        }

        //button zoom
        private void mpOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(60, 60);
        }

        private void mplOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(50, 50);
        }

        private void mpCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(60, 60);
        }

        private void mplCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(50, 50);
        }

        private void dontPress(object sender, KeyPressEventArgs e)
        {
            MessageBox.Show("ไม่สามารถแก้ไขรหัสพนักงานได้","แจ้งเตือน",MessageBoxButtons.OK,MessageBoxIcon.Stop);
        }

        private void dontNumber(object sender, KeyPressEventArgs e)
        {
            /*    if ((e.KeyChar > 47 && e.KeyChar < 58))
                {
                    e.Handled = true;
                    MessageBox.Show("ไม่สามารถกรอกข้อมูลที่เป็นตัวเลขได้", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }*/
            PCValueInput pc = new PCValueInput();
            pc.dontNumber(e);
        }

        private void dontNumber2(object sender, KeyPressEventArgs e)
        {
            PCValueInput pc = new PCValueInput();
            pc.dontNumber(e);
        }
    }
}
